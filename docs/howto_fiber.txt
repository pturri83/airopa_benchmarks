HOWTO
by Paolo Turri
turri@berkeley.edu



These are the instructions to run a test with a fiber grid image.


1  - Create a folder '[test]' where output data will be stored. In it, create a
     'info.txt' file with information like the purpose of the test and details
     on the observation.

2  - In the '$AIROPA_DATA_PATH/ref_files' folder are the reference files used to
     calculate OTF ratio and do profile fitting:
       - airopa.config : AIROPA configuration file
       - 'ratio_atm_corr_*.fits' : Correction file for resampled atmospheric OTF

3  - In the '$AIROPA_DATA_PATH/phase_maps' folder are the phase maps measured on
     the instrument, divided in subfolders by year, with these files in them:
       - cen.fits : Phase map measured at the "image sharpening" (NIRC2
         pointing) position
       - phase_maps_reg.fits : Grid of phase map variations respect to the
         "image sharpening" position
       - x_reg.fits : x positions of the phase map grid
       - y_reg.fits : y positions of the phase map grid

4  - In the '$AIROPA_DATA_PATH/fiber_grids' folder are the reference files used
     to analyze the fiber grid, divided in subfolders by year:
       - fiber.fits : Composie image of a fiber grid
       - psf_stars_fiber.lis : Preliminary catalog of the positions of the
         fibers
       - psf_stars_fiber_force.lis : Preliminary catalog of the positions of the
         fibers, without star names

5  - In '[test]', create a 'pro' subfolder and there copy
     'generate_otf_fiber_run.pro' and 'fiber_fit_run.pro' from the 'source/IDL'
     folder. From 'source/Python' copy also 'prepare_fiber_run.py' and
     'analyze_fiber_run.py'.

6  - To prepare the test, modify the 'prepare_fiber_run.py' file contained in
     the '[test]/pro' subfolder. The parameter is:
       - dir_test : '[test]' folder (absolute path)
       - year : Year of the fiber image
       - n_stars : Number of stars in 'psf_stars_fiber.lis' in the
         '$AIROPA_DATA_PATH/fiber_grids/[year]' folder

7  - Run 'prepare_fiber_run.py' with Python. If the program crashes because it
     can't delete a '._*' file, run it again immediately.

8  - The output files in the '[test]/image' subfolder are:
       - psf_stars_fiber.lis : Preliminary catalog of the positions of the
         fibers
       - psf_stars_fiber_force.lis : Preliminary catalog of the positions of the
         fibers, without star names
       - psf_stars.dat : Catalog of PSF stars in the image
       - fiber.fits : Composie image of a fiber grid
       - fiber.coo : Image coordinates of the reference PSF star

9  - To generate the OTF ratio and PSF grids, modify the
     'generate_otf_fiber_run.pro' file in the '[test]/pro' subfolder. In the
     'generate_otf_fiber_run' procedure, change these parameters:
       - year : Year of the phase maps
       - filter_name : Name of the filter ('H' or 'Kp') as in 'fiber.fits'
       - parang : Parallactic angle as in 'fiber.fits'
       - rotposn : Rotator position as in 'fiber.fits'

10 - In a terminal move to the '[test]/pro' subfolder and launch IDL using the
     'airopa_idl' alias. Compile the program with
     '.compile generate_otf_fiber_run.pro', then, run it with the command
     'generate_otf_fiber_run'.

11 - Instrumental OTF ratio files are created in the folder
     '$AIROPA_DATA_PATH/otf_grids/[year]/[filter_name]/inst/[pa]':
       - otf_ratio_grid_imag.fits : OTF ratio grid (imaginary part)
       - otf_ratio_grid_real.fits : OTF ratio grid (real part)
       - otf_ratio_grid_imag_flat.fits : OTF ratio grid (imaginary part) of a
         flat phase
       - otf_ratio_grid_real_flat.fits : OTF ratio grid (real part) of a flat
         phase
       - params_otf.txt : Information on the relevant parameters used to
         generate the OTF ratio
     The position angle '[pa]' is calculated by 'generate_otf_fiber_run' and
     then saved in 'params_otf.txt'.

12 - Modify the 'fiber_fit_run.pro' file in the '[test]/pro' subfolder. In the
     'fiber_fit_run' procedure, change these parameters:
       - dir_test : As in 'prepare_fiber_run.py'
       - year : As in 'generate_otf_fiber_run.pro'
       - filter_name : As in 'params_otf.txt'
       - parang : As in 'params_otf.txt'
       - rotposn : As in 'params_otf.txt'
       - pa : As in 'params_otf.txt'
       - corr : Correlation value
       - n_stars : As in 'prepare_fiber_run.py'

13 - In a terminal move to the '[test]/pro' subfolder and launch IDL using the
     'airopa_idl' alias. Compile the program with '.compile fiber_fit_run.pro',
     then, run it with the command 'fiber_fit_run'. If an error is caused
     because IDL can't delete a '._*' file, just run it again immidiately.

14 - The output files in the '[test]/fit' subfolder are:
       - params_fit_fiber.txt : Information on the relevant parameters used to
         run AIROPA
       - journal_fit_fiber.txt : IDL journal of the fitting
     The subfolders 'legacy', 'single' and 'variable' are also generated. In
     each of them, the output files are:
       - fiber_back.fits : Background image
       - fiber_psf.fits : PSF image (for 'legacy' and 'single')
       - fiber_on_axis_psf.fits : PSF image (for 'variable')
       - fiber_psf_grid.fits : PSF grid (for 'variable')
       - fiber_res.fits : Residual image
       - fiber_stars.fits : Reconstructed image
       - fiber_0.8_metrics.txt : FVU catalog
       - fiber_0.8_stf.txt : Extracted catalog
       - fiber_0.8_stf.lis : Complete extracted catalog

15 - To analyze the profile fitting output, modify the 'analyze_fiber_run.py'
     file in the '[test]/pro' subfolder. Modify these data parameters:
       - dir_test : As in 'prepare_fiber_run.py'
       - n_fiber : Number of fibers on the image
     Modify the other analysis and plot parameters following the docstring in
     'analyze_fiber_run.py'.

16 - Run 'analyze_fiber_run.py' with Python.

17 - Output from the simulated image are stored in '[test]/plot'. Output plots
     for AIROPA modes 'legacy', 'single' and 'variable' are saved in their
     relative subfolders. Each plot is saved both in PNG (low-quality for quick
     visualization) and PDF format (high-quality for publication) format.
     In '[test]/plot' is also saved the file 'params_sim.txt' with information
     on the relevant parameters used to analyze the plots.

18 - To produce a PDF report for quick analysis of the results, copy the
     'report_fiber.tex' file in the 'latex' subfolder in 'source' into a new
     '[test]/report' folder.

19 - Compile 'report_fiber.tex' with PDFLaTeX. The output 'report_fiber.pdf' is
     generated in '[test]/report'.

20 - Done.
