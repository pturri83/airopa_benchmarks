README
by Paolo Turri
turri@berkeley.edu



This set of programs is designed to test different AIROPA modes on simulated and
on-sky images. On-sky images can be either of the Galctic Center or the mosaic
of nearby masers. The result is a report showing photometry and astrometry
diagnostic plots. It can also be used to calculate a new distortion solution.

More information on NIRC2 angles can be found here:
https://www2.keck.hawaii.edu/inst/nirc2/nirc2_ao.html

AIROPA has to be installed as describe in its readme file, with the correct
'airopa_idl' alias command and 'AIROPA_DATA_PATH' environment variable.

The following software is also required:
 - IDL
 - AIROPA (with StarFinder, Arroyo and AO Optimization submodules)
 - Python 3.6
 - MAOSI
 - FlyStar
 - LaTeX

Add the 'airopa_benchmarks/source/idl' path to the IDL_PATH environment variable
and 'airopa_benchmarks/source/python' path to PYTHONPATH.

If you need to calculate the position of the T/T star respect to GC images,
follow the instructions in 'setup.txt'.

Depending if the analysis is done on simulated or on-sky GC or on-sky maser
images, follow the instructions, respectively, in the 'howto_sim.txt',
'howto_gc.txt' and 'howto_maser.txt' file. If a new distortion solution is
calculated, follow 'howto_distort.txt'.



WARNING: IDL warning messages (e.g., floating underflow, floating overflow,
illegal operand) at the end of an IDL program can and will occur. They are not
catastrophic and should be ignored.

WARNING: Because there are several global variables used in AIROPA, it is safer
to exit IDL and launch it again everytime a new script has to be run.

If you are having any other trouble, don't hesitate to contact me.
