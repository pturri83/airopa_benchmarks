;+
; :Description:
;     Feed 'profile_fit' with arguments.
;
;  :Uses:
;     profile_fit
;-
PRO on_sky_gc_run
  
  ; Data parameters
  dir_test = '/g/lu/scratch/jlu/work/ao/airopa/AIROPA_TEST/' + $
    'airopa_benchmarks/gc_sky'
  dimm_file = '20170823.dimm.dat'
  mass_file = '20170823.masspro.dat'
  
  ; Analysis parameters
  year = 2017
  filter_name = 'Kp'
  force_init = 0
  atm_inst = 'atm'
  
  ; Run AIROPA
  files = FILE_SEARCH(FILEPATH('*.fits', ROOT_DIR=dir_test, SUBDIR='image'))
  n_files = N_ELEMENTS(files)
  FILE_DELETE, FILEPATH(PATH_SEP(), ROOT_DIR=dir_test, SUBDIR='fit'), $
    /RECURSIVE, /ALLOW_NONEXISTENT
  FILE_MKDIR, FILEPATH(PATH_SEP(), ROOT_DIR=dir_test, SUBDIR='fit')
  IF force_init EQ 1 THEN BEGIN
    OPENR, cat_file, FILEPATH('cat.txt', ROOT_DIR=dir_test, SUBDIR='image'), $
      /GET_LUN
    cat_array = FLTARR(3, 1)
    cat_line = FLTARR(3, 1)
    WHILE NOT EOF(cat_file) DO BEGIN
      READF, cat_file, cat_line
      cat_array = [[cat_array], [cat_line]]
    ENDWHILE
    cat_array = cat_array[0: *, 1: *]
    x_force = REFORM(cat_array[0, 0: *])
    y_force = REFORM(cat_array[1, 0: *])
    f_force = REFORM(cat_array[2, 0: *])
  ENDIF
  FOR i_file = 0, (n_files - 1) DO BEGIN
    FITS_READ, files[i_file], image, hdr, /pdu
    rotposn = GET_PA(hdr)
    parang = GET_PARANG(hdr)
    pa = (parang - rotposn) MOD 60
    IF pa LT 0 THEN (pa = pa + 60)
    pa = ROUND(pa)
    filename = FILE_BASENAME(files[i_file], '.fits')
    profile_fit, filename, dir_test, year, filter_name, atm_inst, parang, $
      rotposn, pa, 'irs16C', dimm_file=dimm_file, mass_file=mass_file, $
      on_sky=1, force=force_init, x_force=x_force, y_force=y_force, $
      f_force=f_force
  ENDFOR
END