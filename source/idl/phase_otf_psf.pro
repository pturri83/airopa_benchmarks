;+
; :Description:
;     Generate instrumental OTF and PSF from a defocus phase map.
;
;  :Params:
;     year : in, required, type=INT
;       Year of the phase map
;     psf_size : in, required, type=INT
;       PSF size (px)
;     filter_name : in, required, type=STRING
;       Name of the filter to be used
;     filter_lambda : in, required, type=FLOAT
;       Central wavelength of the filter to be used (m)
;     parang : in, required, type=FLOAT
;       Parallactic angle of the instrument (deg)
;     rotposn : in, required, type=FLOAT
;       Rotator position (deg)
;     grid_overs : in, required, type=INT, default=3
;       OTF grid oversampling
;
; :Uses:
;     airopa_config
;     apply_otf_ratio
;     bool_to_str
;     grid
;     grid_over
;     inst_otf
;     otf_interp
;-
PRO phase_otf_psf
  
  ; Data parameters
  year = 2017
  psf_size = 301
  filter_lambda = 1.6455e-06
  parang = -87.7
  rotposn = 0.7
  grid_overs = 3
  
  ; Point to the AIROPA data folder
  dir = GETENV('AIROPA_DATA_PATH')
  
  ; Prepare the PSF grid
  grid_orig = grid(100., 1024L)
  grid_fine = grid_over(grid_orig, grid_overs)
  
  ; Calculate the OTF
  airopa_config, FILEPATH('airopa_defocus.config', ROOT_DIR=dir, $
    SUBDIR='ref_files'), path = FILEPATH('', ROOT_DIR=dir, $
    SUBDIR=['phase_maps', 'defocus'])
  otf = inst_otf(psf_size, filter_lambda, rotposn, parang, pos1=grid_fine, $
    ref=1, center_pm=1, tt=0, _Extra=create_struct('image_model_ptr', 1))
  
  ; Save files
  dir_otf = FILEPATH('', ROOT_DIR=dir, SUBDIR=['otf_grids', 'defocus'])
  FILE_MKDIR, dir_otf
  FILE_DELETE, FILEPATH('otf_ratio_grid_real.fits', ROOT_DIR=dir_otf), $
    /ALLOW_NONEXISTENT
  writefits, FILEPATH('otf_ratio_grid_real.fits', ROOT_DIR=dir_otf), $
    REAL_PART(otf.otf_ratio)
  FILE_DELETE, FILEPATH('otf_ratio_grid_imag.fits', ROOT_DIR=dir_otf), $
    /ALLOW_NONEXISTENT
  writefits, FILEPATH('otf_ratio_grid_imag.fits', ROOT_DIR=dir_otf), $
    IMAGINARY(otf.otf_ratio)
  dir_psf = FILEPATH('', ROOT_DIR=dir, SUBDIR=['psf_grids', 'defocus'])
  FILE_MKDIR, dir_psf
  FILE_DELETE, FILEPATH('grid_pos.fits', ROOT_DIR=dir_psf), /ALLOW_NONEXISTENT
  writefits, FILEPATH('grid_pos.fits', ROOT_DIR=dir_psf), grid_fine
  FILE_DELETE, FILEPATH('psf_grid.fits', ROOT_DIR=dir_psf), /ALLOW_NONEXISTENT
  writefits, FILEPATH('psf_grid.fits', ROOT_DIR=dir_psf), otf.psf_1
  FILE_DELETE, FILEPATH('psf_grid_cen.fits', ROOT_DIR=dir_psf), $
    /ALLOW_NONEXISTENT
  writefits, FILEPATH('psf_grid_cen.fits', ROOT_DIR=dir_psf), otf.psf_2
END
