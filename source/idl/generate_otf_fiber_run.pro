;+
; :Description:
;     Feed 'profile_fit' with arguments.
;
;  :Uses:
;     generate_otf
;-
PRO generate_otf_fiber_run
  
  ; Analysis parameters
  year = 2017
  filter_name = 'Fe'
  parang = 0.3
  rotposn = 0.7
  
  ; Run AIROPA
  generate_otf, year, filter_name, 'inst', parang, rotposn, psf_generate=0, $
    conf_file='airopa_fiber.config'
END
