;+
; :Description:
;     Feed 'profile_fit' with arguments.
;
;  :Uses:
;     profile_fit
;-
PRO fiber_fit_run
  
  ; Data parameters
  dir_test = '/g/lu/scratch/jlu/work/ao/airopa/AIROPA_TEST/' + $
    'airopa_benchmarks/grid_fiber_17_17'
  
  ; Analysis parameters
  year = 2017
  filter_name = 'Fe'
  parang = 0.3
  rotposn = 0.7
  pa = 60
  corr = 0.85
  n_stars = 81
  
  ; Run AIROPA
  FILE_DELETE, FILEPATH(PATH_SEP(), ROOT_DIR=dir_test, SUBDIR='fit'), $
    /RECURSIVE, /ALLOW_NONEXISTENT
  FILE_MKDIR, FILEPATH(PATH_SEP(), ROOT_DIR=dir_test, SUBDIR='fit')
  read_force, force_arr, dir_test, n_stars
  profile_fit, 'fiber', dir_test, year, filter_name, 'inst', parang, $
    rotposn, pa, 'irs16C', psf_const=0, psf_load=0, dimm_file='', $
    mass_file='', conf_file='airopa_fiber.config', on_sky=0, corr=corr, $
    force=1, x_force=reform(force_arr[2,*]), y_force=reform(force_arr[3,*]), $
    f_force=reform(force_arr[4,*])
END


PRO read_force, force_arr, dir, n_stars
  OPENR, 10, FILEPATH('psf_stars_fiber_force.lis', ROOT_DIR=dir, SUBDIR='image')
  force_arr = FLTARR(8, n_stars)
  READF, 10, force_arr
  CLOSE, 10
END
