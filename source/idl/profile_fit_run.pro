;+
; :Description:
;     Feed 'profile_fit' with arguments.
;
;  :Uses:
;     profile_fit
;-
PRO profile_fit_run
  
  ; Data parameters
  dir_test = '/g/lu/scratch/jlu/work/ao/airopa/AIROPA_TEST/' + $
    'airopa_benchmarks/grid_const'
  
  ; Analysis parameters
  year = 2017
  filter_name = 'Kp'
  atm_inst = 'inst'
  parang = -87.7
  rotposn = 0.7
  pa = 32
  psf_reference = 'irs16C'
  psf_const = 1
  psf_load = 0
  on_sky = 0
  
  ; Atmospheric OTF parameters'
  dimm_file = '20170823.dimm.dat'
  mass_file = '20170823.masspro.dat'
  
  ; Run AIROPA
  psf_const_str = ''
  IF psf_const THEN psf_const_str = '_flat'
  conf_file = 'airopa' + psf_const_str + '.config'
  FILE_DELETE, FILEPATH(PATH_SEP(), ROOT_DIR=dir_test, SUBDIR='fit'), $
    /RECURSIVE, /ALLOW_NONEXISTENT
  FILE_MKDIR, FILEPATH(PATH_SEP(), ROOT_DIR=dir_test, SUBDIR='fit')
  profile_fit, 'img_sim', dir_test, year, filter_name, atm_inst, parang, $
    rotposn, pa, psf_reference, psf_const=psf_const, psf_load=psf_load, $
    dimm_file=dimm_file, mass_file=mass_file, conf_file=conf_file, on_sky=on_sky
END
