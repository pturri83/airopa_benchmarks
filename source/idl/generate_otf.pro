;+
; :Description:
;     Generate an OTF map with AIROPA from a phase map.
;
;  :Params:
;     year : in, required, type=INT
;       Year of the phase map
;     filter_name : in, required, type=STRING
;       Name of the filter to be used
;     atm_inst : in, required, type=STRING
;       Atmospheric ('atm') or instrument-only ('inst') OTF
;     parang : in, required, type=FLOAT
;       Parallactic angle of the instrument (deg)
;     rotposn : in, required, type=FLOAT
;       Rotator position (deg)
;
; :Keywords:
;     grid_overs : in, required, type=INT, default=3
;       OTF grid oversampling
;     psf_size : in, required, type=INT, default=301
;       PSF size (px)
;     psf_generate : in, optional, type=INT, default=1
;       Generate PSF grid (0 or 1)
;     dimm_file : in, required, type=STRING
;       DIMM file name
;     mass_file : in, required, type=STRING
;       MASS file name
;     ttgs_x : in, optional, type=FLOAT, default=-392
;       Coordinate x of the tip-tilt star
;     ttgs_y : in, optional, type=FLOAT, default=2463
;       Coordinate y of the tip-tilt star
;     zenang : in, optional, type=FLOAT, default=45
;       Zenith angle
;     lgsalt : in, optional, type=FLOAT, default=90
;       LGS altitude (km)
;     date_atm : in, optional, type=STRING
;       Date and time of the observation ('yyyy-mm-dd hh:mm:ss')
;     atm_corr_file : in, optional, type=STRING
;       File with the correction for resampled atmospheric OTF
;     dir_test : in, optional, type=STRING
;       Directory of the atmospheric test
;     part_size : in, optional, type=INT, default=102
;       Grid pitch (px)
;     conf_file: in, required, type=STRING
;       AIROPA config file name
;
; :Uses:
;     airopa_config
;     apply_otf_ratio
;     bool_to_str
;     grid
;     grid_over
;     inst_otf
;     otf_interp
;-
PRO generate_otf, year, filter_name, atm_inst, parang, rotposn, $
  grid_overs=grid_overs, psf_size=psf_size, psf_generate=psf_generate, $
  dimm_file=dimm_file, mass_file=mass_file, ttgs_x=ttgs_x, ttgs_y=ttgs_y, $
  zenang=zenang, lgsalt=lgsalt, date_atm=date_atm, part_size=part_size, $
  atm_corr_file=atm_corr_file, dir_test=dir_test, conf_file=conf_file
  
  ; Set the keywords' default values
  IF NOT KEYWORD_SET(grid_overs) THEN (grid_overs = 3)
  IF NOT KEYWORD_SET(psf_size) THEN (psf_size = 301)
  IF N_ELEMENTS(psf_generate) EQ 0 THEN (psf_generate = 1)
  IF NOT KEYWORD_SET(ttgs_x) THEN (ttgs_x = -371)
  IF NOT KEYWORD_SET(ttgs_y) THEN (ttgs_y = 2232)
  IF N_ELEMENTS(zenang) EQ 0 THEN (zenang = 45)
  IF NOT KEYWORD_SET(lgsalt) THEN (lgsalt = 90)
  IF NOT KEYWORD_SET(part_size) THEN (part_size = 102)
  IF NOT KEYWORD_SET(atm_corr_file) THEN (atm_corr_file = $
    'ratio_atm_corr_k_101_to_301.fits')
  IF NOT KEYWORD_SET(conf_file) THEN (conf_file = 'airopa.config')
  
  ; Select the filter wavelength
  IF STRCMP(filter_name, 'H') THEN (filter_lambda = 1.633e-06)
  IF STRCMP(filter_name, 'Fe') THEN (filter_lambda = 1.646e-06)
  IF STRCMP(filter_name, 'Kp') THEN (filter_lambda = 2.1245e-06)
  
  ; Point to the AIROPA data folder
  dir = GETENV('AIROPA_DATA_PATH')
  
  ; Prepare the PSF grid
  grid_orig = grid(100., 1024L)
  grid_fine = grid_over(grid_orig, grid_overs)
  
  ; Generate a header file
  pa = (parang - rotposn) MOD 60
  IF pa LT 0 THEN (pa = pa + 60)
  pa = ROUND(pa)
  CALDAT, SYSTIME(/JULIAN), month_cal, day_cal, year_cal
  head_txt = $
    'Date analyzed:            ' + STRTRIM(STRING(year_cal, FORMAT='(I)'), 1) $
    + '-' + STRTRIM(STRING(month_cal, FORMAT='(I)'), 1) + '-' + $
    STRTRIM(STRING(day_cal, FORMAT='(I)'), 1) + STRING(10B) + STRING(10B) + $
    '--- PARAMETERS ---' + STRING(10B) + $
    'Phase map year:           ' + STRTRIM(STRING(year), 1) + STRING(10B) + $
    'Filter:                   ' + filter_name + STRING(10B) + $
    'Parallactic angle (deg):  ' + STRTRIM(STRING(parang, FORMAT='(F5.1)'), 1) $
    + STRING(10B) + $
    'Rotator position (deg):   ' + $
    STRTRIM(STRING(rotposn, FORMAT='(F5.1)'), 1) + STRING(10B) + $
    'Position angle (deg):     ' + STRTRIM(STRING(pa), 1) + STRING(10B) + $
    'Zenith angle (deg):       ' + STRTRIM(STRING(zenang), 1) + STRING(10B) + $
    'LGS altitude (km):        ' + STRTRIM(STRING(lgsalt), 1) + STRING(10B) + $
    'Grid oversampling:        ' + STRTRIM(STRING(grid_overs), 1)
  IF STRCMP(atm_inst, 'atm') THEN BEGIN
    head_txt = head_txt + STRING(10B) + $
    'Atm OTF correction file:  ' + atm_corr_file + STRING(10B) + $
    'DIMM file:                ' + dimm_file + STRING(10B) + $
    'MASS file:                ' + mass_file + STRING(10B) + $
    'T/T NGS position:         ' + STRTRIM(STRING(ttgs_x, FORMAT='(F0.2)'), 1) $
    + ', ' + STRTRIM(STRING(ttgs_y, FORMAT='(F0.2)'), 1)
  ENDIF
  FILE_MKDIR, FILEPATH(PATH_SEP(), ROOT_DIR=GETENV('AIROPA_DATA_PATH'), $
    SUBDIR=['otf_grids', STRTRIM(STRING(year), 1), filter_name, 'inst', $
    STRTRIM(STRING(pa), 1)])
  OPENW, 2, FILEPATH('params_otf.txt', ROOT_DIR=dir, SUBDIR=['otf_grids', $
    STRTRIM(STRING(year), 1), filter_name, 'inst', STRTRIM(STRING(pa), 1)])
  PRINTF, 2, head_txt
  CLOSE, 2
  IF psf_generate THEN BEGIN
    head_txt = head_txt + STRING(10B) + $
      'PSF size:                 ' + STRTRIM(STRING(psf_size), 1)
    FILE_MKDIR, FILEPATH(PATH_SEP(), ROOT_DIR=GETENV('AIROPA_DATA_PATH'), $
      SUBDIR=['psf_grids', STRTRIM(STRING(year), 1), filter_name, atm_inst, $
      STRTRIM(STRING(pa), 1)])
    OPENW, 3, FILEPATH('params_psf.txt', ROOT_DIR=dir, SUBDIR=['psf_grids', $
      STRTRIM(STRING(year), 1), filter_name, atm_inst, STRTRIM(STRING(pa), 1)])
    PRINTF, 3, head_txt
    CLOSE, 3
  ENDIF
  
  ; Calculate the OTF
  airopa_config, FILEPATH(conf_file, ROOT_DIR=dir, SUBDIR='ref_files'), $
    path = FILEPATH('', ROOT_DIR=dir, SUBDIR=['phase_maps', $
    STRTRIM(STRING(year), 1)])
  instr_otf = inst_otf(psf_size, filter_lambda, rotposn, parang, $
    pos1=grid_fine, ref=1, center_pm=1, tt=0, $
    _Extra=create_struct('image_model_ptr', 1))
  IF STRCMP(atm_inst, 'atm') THEN BEGIN
    atm_struct = {image_size: 1024, psf_size: psf_size, grid_size: part_size, $
                  grid_over: grid_overs, lambda: filter_lambda, $
                  rotposn: rotposn, parang: parang, zen: FLOAT(zenang), $
                  lgsalt: FLOAT(lgsalt), date: date_atm, ttgs_x: ttgs_x, $
                  ttgs_y: ttgs_y, lgs_x: 511.5, lgs_y: 511.5, $
                  mass_file: $
                  FILEPATH(mass_file, ROOT_DIR=dir_test, SUBDIR='image'), $
                  dimm_file: $
                  FILEPATH(dimm_file, ROOT_DIR=dir_test, SUBDIR='image')}
    grid_atm = GRID(atm_struct.grid_size, atm_struct.image_size)
    atm_struct = create_struct('grid', grid_atm, atm_struct)
    atm_corr = readfits(FILEPATH(atm_corr_file, $
      ROOT_DIR=GETENV('AIROPA_DATA_PATH')))
    atmo_otf = atm_otf(atm_struct, $
      config_file=FILEPATH(conf_file, ROOT_DIR=dir, SUBDIR='ref_files'), $
      corr=atm_corr, filter_struct=instr_otf.filter)
    atmo_otf_fine = otf_interp(atmo_otf.ratio_array, atm_struct.grid_over)
  ENDIF
  airopa_config, FILEPATH('airopa_flat.config', ROOT_DIR=dir, $
    SUBDIR='ref_files'), path = FILEPATH('', ROOT_DIR=dir, $
    SUBDIR=['phase_maps', STRTRIM(STRING(year), 1)])
  result_otf_flat = inst_otf(psf_size, filter_lambda, rotposn, parang, $
    pos1=grid_fine, ref=1, center_pm=1, tt=0, $
    _Extra=create_struct('image_model_ptr', 1))
  
  ; Calculate the PSF
  IF (psf_generate EQ 1) THEN BEGIN
    model_psf = readfits(FILEPATH(('psf_model_' + filter_name + '_' + $
      STRTRIM(STRING(parang, FORMAT='(F5.1)'), 1) + '_' + $
      STRTRIM(STRING(rotposn, FORMAT='(F5.1)'), 1) + '.fits'), ROOT_DIR=dir, $
      SUBDIR='ref_files'))
    IF STRCMP(atm_inst, 'inst') THEN BEGIN
      psf_grid = apply_otf_ratio(model_psf, instr_otf.otf_ratio)
    ENDIF ELSE BEGIN
      psf_grid = apply_otf_ratio(model_psf, instr_otf.otf_ratio * atmo_otf_fine)
    ENDELSE
    size_grid1 = SIZE(psf_grid)
    size_grid2 = (size_grid1[3] / 2)
    psf_single = psf_grid[*, *, size_grid2]
    psf_grid_flat = apply_otf_ratio(model_psf, result_otf_flat.otf_ratio)
    psf_single_flat = psf_grid_flat[*, *, size_grid2]
  ENDIF
  
  ; Save files
  dir_otf = FILEPATH('', ROOT_DIR=dir, SUBDIR=['otf_grids', $
    STRTRIM(STRING(year), 1), filter_name, 'inst', STRTRIM(STRING(pa), 1)])
  FILE_MKDIR, dir_otf
  FILE_DELETE, FILEPATH('otf_ratio_grid_real.fits', ROOT_DIR=dir_otf), $
    /ALLOW_NONEXISTENT
  writefits, FILEPATH('otf_ratio_grid_real.fits', ROOT_DIR=dir_otf), $
    REAL_PART(instr_otf.otf_ratio)
  FILE_DELETE, FILEPATH('otf_ratio_grid_imag.fits', ROOT_DIR=dir_otf), $
    /ALLOW_NONEXISTENT
  writefits, FILEPATH('otf_ratio_grid_imag.fits', ROOT_DIR=dir_otf), $
    IMAGINARY(instr_otf.otf_ratio)
  FILE_DELETE, FILEPATH('otf_ratio_grid_real_flat.fits', ROOT_DIR=dir_otf), $
    /ALLOW_NONEXISTENT
  writefits, FILEPATH('otf_ratio_grid_real_flat.fits', ROOT_DIR=dir_otf), $
    REAL_PART(result_otf_flat.otf_ratio)
  FILE_DELETE, FILEPATH('otf_ratio_grid_imag_flat.fits', ROOT_DIR=dir_otf), $
    /ALLOW_NONEXISTENT
  writefits, FILEPATH('otf_ratio_grid_imag_flat.fits', ROOT_DIR=dir_otf), $
    IMAGINARY(result_otf_flat.otf_ratio)
  dir_psf = FILEPATH('', ROOT_DIR=dir, SUBDIR=['psf_grids', $
    STRTRIM(STRING(year), 1), filter_name, atm_inst, STRTRIM(STRING(pa), 1)])
  FILE_MKDIR, dir_psf
  FILE_DELETE, FILEPATH('grid_pos.fits', ROOT_DIR=dir_psf), /ALLOW_NONEXISTENT
  writefits, FILEPATH('grid_pos.fits', ROOT_DIR=dir_psf), grid_fine
  IF psf_generate THEN BEGIN
    FILE_DELETE, FILEPATH('psf_grid.fits', ROOT_DIR=dir_psf), /ALLOW_NONEXISTENT
    writefits, FILEPATH('psf_grid.fits', ROOT_DIR=dir_psf), psf_grid
    FILE_DELETE, FILEPATH('psf_single.fits', ROOT_DIR=dir_psf), $
      /ALLOW_NONEXISTENT
    writefits, FILEPATH('psf_single.fits', ROOT_DIR=dir_psf), psf_single
    IF STRCMP(atm_inst, 'inst') THEN BEGIN
      FILE_DELETE, FILEPATH('psf_grid_flat.fits', ROOT_DIR=dir_psf), $
        /ALLOW_NONEXISTENT
      writefits, FILEPATH('psf_grid_flat.fits', ROOT_DIR=dir_psf), psf_grid_flat
      FILE_DELETE, FILEPATH('psf_single_flat.fits', ROOT_DIR=dir_psf), $
        /ALLOW_NONEXISTENT
      writefits, FILEPATH('psf_single_flat.fits', ROOT_DIR=dir_psf), $
        psf_single_flat
    ENDIF
  ENDIF
END
