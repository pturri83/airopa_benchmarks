;+
; :Description:
;     Feed 'generate_otf' with arguments.
;
;  :Uses:
;     generate_otf
;-
PRO generate_otf_run
  
  ; Analysis parameters
  year = 2017
  filter_name = 'Kp'
  atm_inst = 'inst'
  parang = -87.7
  rotposn = 0.7
  
  ; Atmospheric OTF parameters
  dir_test = '/g/lu/scratch/jlu/work/ao/airopa/AIROPA_TEST/' + $
    'airopa_benchmarks/grid_const'
  dimm_file = '20170823.dimm.dat'
  mass_file = '20170823.masspro.dat'
  zenang = 45
  date_atm = '2017-08-23 07:00:00'
  
  ; Run AIROPA
  generate_otf, year, filter_name, atm_inst, parang, rotposn, $
    dimm_file=dimm_file,  mass_file=mass_file, zenang=zenang, $
    date_atm=date_atm, dir_test=dir_test
END
