;+
; :Description:
;     Feed 'profile_fit' with arguments.
;
;  :Uses:
;     profile_fit
;-
PRO on_sky_maser_run
  
  ; Data parameters
  dir_test = '/g/lu/scratch/jlu/work/ao/ao_optimization/AIROPA_TEST/' + $
    'airopa_benchmarks/maser'
  atm_corr_file = 'ratio_atm_corr_k_101_to_301.fits'
  dimm_files = ['20170506.dimm.dat', '20170507.dimm.dat', '20170506.dimm.dat', $
    '20170506.dimm.dat', '20170506.dimm.dat']
  mass_files = ['20170506.masspro.dat', '20170507.masspro.dat', $
    '20170506.masspro.dat', '20170506.masspro.dat', '20170506.masspro.dat']
  
  ; Analysis parameters
  otf_generate = 1
  year = 2017
  filter_name = 'Kp'
  force_init = 0
  psf_stars = ['irs16C', 'irs16NW', 'S12-1', 'irs16C', 'irs1SE']
  ttgs_xs = [47.68, -267.17, 343.38, -247.15, 341.55]
  ttgs_ys = [2210.81, 2041.62, 2547.35, 2515.32, 2314.95]
  
  ; Run AIROPA
  mosaic_name = ['ce', 'nw', 'ne', 'sw', 'se']
  files = FILE_SEARCH(FILEPATH('*.fits', ROOT_DIR=dir_test, SUBDIR='image'))
  n_files = N_ELEMENTS(files)
  FILE_DELETE, FILEPATH(PATH_SEP(), ROOT_DIR=dir_test, SUBDIR='fit'), $
    /RECURSIVE, /ALLOW_NONEXISTENT
  FILE_MKDIR, FILEPATH(PATH_SEP(), ROOT_DIR=dir_test, SUBDIR='fit')
  IF force_init EQ 1 THEN BEGIN
    OPENR, cat_file, FILEPATH('cat.txt', ROOT_DIR=dir_test, SUBDIR='image'), $
      /GET_LUN
    cat_array = FLTARR(3, 1)
    cat_line = FLTARR(3, 1)
    WHILE NOT EOF(cat_file) DO BEGIN
      READF, cat_file, cat_line
      cat_array = [[cat_array], [cat_line]]
    ENDWHILE
    cat_array = cat_array[0: *, 1: *]
    x_force = REFORM(cat_array[0, 0: *])
    y_force = REFORM(cat_array[1, 0: *])
    f_force = REFORM(cat_array[2, 0: *])
  ENDIF
  FOR i_file = 0, (n_files - 1) DO BEGIN
    FITS_READ, files[i_file], image, hdr, /pdu
    rotposn = GET_PA(hdr)
    parang = GET_PARANG(hdr)
    pa = (parang - rotposn) MOD 60
    IF pa LT 0 THEN (pa = pa + 60)
    pa = ROUND(pa)
    IF (otf_generate EQ 1) THEN BEGIN
      generate_otf, year, filter_name, 'inst', parang, rotposn, psf_generate=0
    ENDIF
    filename = FILE_BASENAME(files[i_file], '.fits')
    IF STRCMP(filename.SUBSTRING(0, 1), mosaic_name[0]) THEN BEGIN
      mosaic_idx = 0
    ENDIF ELSE IF STRCMP(filename.SUBSTRING(0, 1), mosaic_name[1]) THEN BEGIN
      mosaic_idx = 1
    ENDIF ELSE IF STRCMP(filename.SUBSTRING(0, 1), mosaic_name[2]) THEN BEGIN
      mosaic_idx = 2
    ENDIF ELSE IF STRCMP(filename.SUBSTRING(0, 1), mosaic_name[3]) THEN BEGIN
      mosaic_idx = 3
    ENDIF ELSE IF STRCMP(filename.SUBSTRING(0, 1), mosaic_name[4]) THEN BEGIN
      mosaic_idx = 4
    ENDIF
    psf_star = psf_stars[mosaic_idx]
    dimm_file = dimm_files[mosaic_idx]
    mass_file = mass_files[mosaic_idx]
    ttgs_x = ttgs_xs[mosaic_idx]
    ttgs_y = ttgs_ys[mosaic_idx]
    profile_fit, filename, dir_test, year, filter_name, 'atm', parang, $
      rotposn, pa, psf_star, dimm_file=dimm_file, mass_file=mass_file, $
      atm_corr_file=atm_corr_file, otf_load=0, ttgs_x=ttgs_x, ttgs_y=ttgs_y, $
      on_sky=1, force=force_init, x_force=x_force, y_force=y_force, $
      f_force=f_force
  ENDFOR
END