;+
; :Description:
;     Transform a zero or a positive integer value to a "no" or "yes" string,
;       respectively.
;
; :Params:
;     bool_val : in, required, type=INT
;       Boolean value to transform (0 or positive integer)
;     bool_str : out, type=STRING
;       Transformed Boolean value ("no" or "yes")
;-
FUNCTION bool_to_str, bool_val

  ; Transform the Boolean into a string
  IF bool_val THEN (bool_str = "yes") ELSE (bool_str = "no")

  RETURN, bool_str
END
