from os import path

from maosi.iris import ao_opt_sims


def convert_psf(dir_test, dir_psf, psf_root):

    # Start the program
    print("\nConversion started")

    # Convert the PSF
    ao_opt_sims.convert_iris_psf_grid(path.join(dir_psf, psf_root),
                                      path.join(dir_test, 'image'))

    # End program
    print("Conversion completed\n")
