"""Test instrumental OTF and PSF generation by AIROPA of a defocus phase map.

Data parameters:
    fiber_pos (int [2]) - Position of the fiber image to be analyzed (px)
        ([x, y])
    fiber_buffer (int) - Half-buffer of the fiber image to be analyzed (px)
    psf_buffer (int) - Half-buffer of the PSF image to be analyzed (px)
    image_max (float) - Maximum value to color in the PSF
    pupil_shape (str) - Shape of the aperture mask
    lambd (float) - Central wavelength of the band used (m)

Fixed parameters:
    diams (float [2]) -Aperture diameters (m) ([Keck, DM])
    p_size (float) - NIRC2 pixel size (m)
    n_side (int) - Size of the array over which the aperture is defined (px)
    ps (float) - NIRC2 pixel scale ("/px)
    f_ff (float) - Front focus focal length (m)
"""

from os import environ, path

from astropy.io import fits
from matplotlib import colors, pyplot
import numpy as np
from scipy import optimize


# Define the Gaussian function
def gauss(height, x0, y0, sigmax, sigmay):
    return lambda x, y: height * np.exp(-((((x0 - x) / sigmax) ** 2) +
                                          (((y0 - y) / sigmay) ** 2)) / 2)


# Calculate the Gaussian parameters calculating its moments
def gauss_params(data):
    total = data.sum()
    x1, y1 = np.indices(data.shape)
    x2 = (x1 * data).sum() / total
    y2 = (y1 * data).sum() / total
    col = data[:, int(y2)]
    row = data[int(x2), :]
    sigmax = np.sqrt(abs(((np.arange(col.size) - y2) ** 2) * col).sum() /
                     col.sum())
    sigmay = np.sqrt(abs(((np.arange(row.size) - x2) ** 2) * row).sum() /
                     row.sum())
    height = data.max()
    return height, x2, y2, sigmax, sigmay


# Fit the Gaussian
def gauss_fit(data):
    params = gauss_params(data)
    errorfunction = lambda p: \
        np.ravel(gauss(*p)(*np.indices(data.shape)) - data)
    p, success = optimize.leastsq(errorfunction, params)
    return p


def gauss1d(x, p0, p1, p2):
    return p0 * np.exp(-(x - p1) ** 2 / (2. * p2 ** 2))


# Data parameters
fiber_pos = [407, 509]
fiber_buffer = 30
psf_buffer = 26
image_max = 0.1
pupil_shape = 'dm'  # 'keck' # 'dm'
lambd = 1.6455e-6  # 2.1245e-6 # 1.6455e-6

# Fixed parameters
diams = [10.67, 11.14]
p_size = 27e-6
n_side = 1024
ps = 0.009952
f_ff = 149.6

# Load data
print('Program started')
dir_airopa = environ['AIROPA_DATA_PATH']
psf_cen = fits.open(path.join(dir_airopa, 'psf_grids', 'defocus',
                              'psf_grid_cen.fits'))
psf = fits.open(path.join(dir_airopa, 'psf_grids', 'defocus', 'psf_grid.fits'))
mm0 = fits.open(path.join(dir_airopa, 'fiber_grids', '2017', '0mm.fits'))
mm2 = fits.open(path.join(dir_airopa, 'fiber_grids', '2017', '2mm.fits'))
mm4 = fits.open(path.join(dir_airopa, 'fiber_grids', '2017', '4mm.fits'))

# Show PSF
psf_cen = psf_cen[0].data
psf_side = psf_cen.shape[0]
pyplot.figure(figsize=(6, 6))
pyplot.imshow(psf_cen, cmap='jet', aspect='equal',
              norm=colors.LogNorm(vmin=(image_max * 1e-4), vmax=image_max))
pyplot.xlim([(((psf_cen.shape[1] - 1) / 2) - psf_buffer),
             (((psf_cen.shape[1] - 1) / 2) + psf_buffer)])
pyplot.ylim([(((psf_cen.shape[0] - 1) / 2) - psf_buffer),
             (((psf_cen.shape[0] - 1) / 2) + psf_buffer)])
psf_one = psf[0].data[0, :, :]
pyplot.figure(figsize=(6, 6))
pyplot.imshow(psf_one, cmap='jet', aspect='equal',
              norm=colors.LogNorm(vmin=(image_max * 1e-4), vmax=image_max))
pyplot.xlim([(((psf_one.shape[1] - 1) / 2) - psf_buffer),
             (((psf_one.shape[1] - 1) / 2) + psf_buffer)])
pyplot.ylim([(((psf_one.shape[0] - 1) / 2) - psf_buffer),
             (((psf_one.shape[0] - 1) / 2) + psf_buffer)])
print("SR: {0:.2f}".format(np.max(psf_one) / np.max(psf_cen)))

# Show fibers
mm0_one = mm0[0].data[int(fiber_pos[1] - fiber_buffer):
                      int(fiber_pos[1] + fiber_buffer),
                      int(fiber_pos[0] - fiber_buffer):
                      int(fiber_pos[0] + fiber_buffer)]
pyplot.figure(figsize=(6, 6))
pyplot.imshow(mm0_one, cmap='jet', aspect='equal')
mm2_one = mm2[0].data[int(fiber_pos[1] - fiber_buffer):
                      int(fiber_pos[1] + fiber_buffer),
                      int(fiber_pos[0] - fiber_buffer):
                      int(fiber_pos[0] + fiber_buffer)]
pyplot.figure(figsize=(6, 6))
pyplot.imshow(mm2_one, cmap='jet', aspect='equal')
mm4_one = mm4[0].data[int(fiber_pos[1] - fiber_buffer):
                      int(fiber_pos[1] + fiber_buffer),
                      int(fiber_pos[0] - fiber_buffer):
                      int(fiber_pos[0] + fiber_buffer)]
pyplot.figure(figsize=(6, 6))
pyplot.imshow(mm4_one, cmap='jet', aspect='equal')

# Measure PSF
diam = 0

if pupil_shape == 'keck':
    diam = diams[0]
elif pupil_shape == 'dm':
    diam = diams[1]

fn_ff = f_ff / diam
fit_params_psf_cen = gauss_fit(psf_cen)
fwhm_psf_cen = 2 * np.sqrt(2 * np.log(2)) * np.mean(fit_params_psf_cen[3:])
print("Fitted FWHM of the central PSF: {0:.2f} mas".format(fwhm_psf_cen * ps *
                                                           1e3))
x_grid, y_grid = np.meshgrid(np.arange(-((psf_side - 1) / 2),
                                       (((psf_side - 1) / 2) + 1), 1),
                             np.arange(-((psf_side - 1) / 2),
                                       (((psf_side - 1) / 2) + 1), 1))
psf_rad = psf_one.flatten()
rad = np.hypot(x_grid, y_grid).flatten()
pyplot.figure(figsize=(6, 6))
pyplot.scatter(rad, psf_rad, color='k')
pyplot.xlim([0, 20])
pyplot.ylim([0, (np.max(psf_one) * 1.3)])
idx_fit = np.where((rad > (rad[np.where(psf_rad ==
                                        np.max(psf_one))[0][0]] - 2)) &
                   (rad < (rad[np.where(psf_rad ==
                                        np.max(psf_one))[0][0]] + 2)))[0]
coeff1d, _ = optimize.curve_fit(gauss1d, rad[idx_fit], psf_rad[idx_fit])
fwhm2 = 2 * (coeff1d[1] + (np.sqrt(2 * np.log(2)) * coeff1d[2])) * ps
print("Fitted FWHM of the PSF: {0:.2f} mas".format(fwhm2 * 1e3))

# Measure fiber
mm_side = mm0_one.shape[0]
mm0_one_fit = mm0_one[int((mm_side / 2) - 10): int((mm_side / 2) + 10),
              int((mm_side / 2) - 10): int((mm_side / 2) + 10)]
fit_params_mm0 = gauss_fit(mm0_one_fit)
fwhm_mm0 = 2 * np.sqrt(2 * np.log(2)) * np.mean(fit_params_mm0[3:])
print("Fitted FWHM of the 0mm fiber: {0:.2f} mas".format(fwhm_mm0 * ps * 1e3))
mm2_one_fit = mm2_one[int((mm_side / 2) - 10): int((mm_side / 2) + 10),
              int((mm_side / 2) - 10): int((mm_side / 2) + 10)]
mm2_one_side = mm2_one_fit.shape[0]
x_grid2, y_grid2 = np.meshgrid((np.arange(0, mm2_one_side, 1) -
                                fit_params_mm0[2]),
                               (np.arange(0, mm2_one_side, 1) -
                                fit_params_mm0[1]))
psf_rad2 = mm2_one_fit.flatten()
rad2 = np.hypot(x_grid2, y_grid2).flatten()
pyplot.figure(figsize=(6, 6))
pyplot.scatter(rad2, psf_rad2, color='k')
pyplot.xlim([0, 20])
pyplot.ylim([0, (np.max(mm2_one_fit) * 1.3)])

# End program
psf.close()
pyplot.show()
print('Program terminated')
