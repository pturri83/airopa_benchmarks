"""Plot the NIRC2 images of a fiber (0, 2, 4, and 6 mm).

Data parameters:
    dir_fits (str) - Folder with the fiber images
    fiber_pos (list) - Approximate position of the fiber (px)

Graphic parameters:
    fiber_buffer (int) - Buffer of the fiber images (px)
    fiber_normalize (list) - Normalization value of the fiber images (0, 2, 4, 6
        mm)
    fiber_min (list) - Minimum value of the fiber images displayed (0, 2, 4, 6
        mm)
"""


from os import path

from astropy.io import fits
from matplotlib import colors, pyplot
import numpy as np


# Data parameters
dir_fits = '/g/lu/data/airopa_data/ref_files'
fiber_pos = [475, 200]

# Graphic parameters
fiber_buffer = 100
fiber_normalize = [200, 200, 150, 100]
fiber_min = [200, 250, 150, 100]

# Load data
print('Program started')
mm0 = fits.open(path.join(dir_fits, '0mm.fits'))
mm2 = fits.open(path.join(dir_fits, '2mm.fits'))
mm4 = fits.open(path.join(dir_fits, '4mm.fits'))
mm6 = fits.open(path.join(dir_fits, '6mm.fits'))
mm0 = mm0[0].data
mm2 = mm2[0].data
mm4 = mm4[0].data
mm6 = mm6[0].data

# Process data
mm0 = mm0 + fiber_normalize[0]
mm2 = mm2 + fiber_normalize[1]
mm4 = mm4 + fiber_normalize[2]
mm6 = mm6 + fiber_normalize[3]

# Show plots
pyplot.figure(figsize=(6, 6))
pyplot.imshow(mm0, cmap='jet', aspect='equal',
              norm=colors.LogNorm(vmin=fiber_min[0], vmax=np.max(mm0)))
pyplot.xlim([(fiber_pos[0] - fiber_buffer), (fiber_pos[0] + fiber_buffer)])
pyplot.ylim([(fiber_pos[1] - fiber_buffer), (fiber_pos[1] + fiber_buffer)])
pyplot.tick_params(axis='both', which='both', bottom=False, left=False,
                   labelbottom=False, labelleft=False)

pyplot.figure(figsize=(6, 6))
pyplot.imshow(mm2, cmap='jet', aspect='equal',
              norm=colors.LogNorm(vmin=fiber_min[1], vmax=np.max(mm2)))
pyplot.xlim([(fiber_pos[0] - fiber_buffer), (fiber_pos[0] + fiber_buffer)])
pyplot.ylim([(fiber_pos[1] - fiber_buffer), (fiber_pos[1] + fiber_buffer)])
pyplot.tick_params(axis='both', which='both', bottom=False, left=False,
                   labelbottom=False, labelleft=False)

pyplot.figure(figsize=(6, 6))
pyplot.imshow(mm4, cmap='jet', aspect='equal',
              norm=colors.LogNorm(vmin=fiber_min[2], vmax=np.max(mm4)))
pyplot.xlim([(fiber_pos[0] - fiber_buffer), (fiber_pos[0] + fiber_buffer)])
pyplot.ylim([(fiber_pos[1] - fiber_buffer), (fiber_pos[1] + fiber_buffer)])
pyplot.tick_params(axis='both', which='both', bottom=False, left=False,
                   labelbottom=False, labelleft=False)

pyplot.figure(figsize=(6, 6))
pyplot.imshow(mm6, cmap='jet', aspect='equal',
              norm=colors.LogNorm(vmin=fiber_min[3], vmax=np.max(mm6)))
pyplot.xlim([(fiber_pos[0] - fiber_buffer), (fiber_pos[0] + fiber_buffer)])
pyplot.ylim([(fiber_pos[1] - fiber_buffer), (fiber_pos[1] + fiber_buffer)])
pyplot.tick_params(axis='both', which='both', bottom=False, left=False,
                   labelbottom=False, labelleft=False)

# End program
pyplot.show()
print('Program terminated')
