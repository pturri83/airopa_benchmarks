"""Generate a defocus phase map to test instrumental OTF and PSF generation by
AIROPA.

Data parameters:
    dir_year (str) - Year of the model phase folder
    coeff (float) - Defocus Zernike coefficient (nm)
    fiber_illumination (float) - Width (sigma) of the Gaussian illumination
        pattern of the pupil (px)
    psf_buffer (int) - Half-buffer of the simulated PSF to be analyzed (px)
    pupil_shape (str) - Shape of the aperture mask
    lambd (float) - Central wavelength of the band used (m)

Fixed parameters:
    diams (float [2]) - Aperture diameters (m) ([Keck, DM])
    diam_apertures (int [2]) - Aperture diameters (px) ([Keck, DM])
    p_size (float) - NIRC2 pixel size (m)
    n_side (int) - Size of the array over which the aperture is defined (px)
    ps (float) - NIRC2 pixel scale ("/px)
    f_ff (float) - Front focus focal length (m)
"""

import copy
from os import environ, makedirs, path
import shutil

from astropy.io import fits
from matplotlib import colors, pyplot
import numpy as np
from scipy import optimize


# Define the Gaussian function
def gauss(height, x0, y0, sigmax, sigmay):
    return lambda x, y: height * np.exp(-((((x0 - x) / sigmax) ** 2) +
                                          (((y0 - y) / sigmay) ** 2)) / 2)


# Calculate the Gaussian parameters calculating its moments
def gauss_params(data):
    total = data.sum()
    x1, y1 = np.indices(data.shape)
    x2 = (x1 * data).sum() / total
    y2 = (y1 * data).sum() / total
    col = data[:, int(y2)]
    row = data[int(x2), :]
    sigmax = np.sqrt(abs(((np.arange(col.size) - y2) ** 2) * col).sum() /
                     col.sum())
    sigmay = np.sqrt(abs(((np.arange(row.size) - x2) ** 2) * row).sum() /
                     row.sum())
    height = data.max()
    return height, x2, y2, sigmax, sigmay


# Fit the Gaussian
def gauss_fit(data):
    params = gauss_params(data)
    errorfunction = lambda p: np.ravel(gauss(*p)(*np.indices(data.shape)) -
                                       data)
    p, success = optimize.leastsq(errorfunction, params)
    return p

def gauss1d(x, p0, p1, p2):
    return p0 * np.exp(-(x - p1) ** 2 / (2. * p2 ** 2))


# Data parameters
dir_year = '2017'
coeff = 400
fiber_illumination = None  # None # 50
psf_buffer = 30
pupil_shape = 'dm'  # 'keck' # 'dm'
lambd = 1.6455e-6  # 2.1245e-6 # 1.6455e-6

# Fixed parameters
diams = [10.67, 11.14]
diam_apertures = [200, 209]
p_size = 27e-6
n_side = 1024
ps = 0.009952
f_ff = 149.6

# Load data
print('Program started')
dir_airopa1 = environ['AIROPA_DATA_PATH']
dir_airopa2 = environ['AIROPA_TEST_DATA']
phase_orig = fits.open(path.join(dir_airopa1, 'phase_maps', dir_year,
                                 'phase_maps_reg.fits'))
phase_size = [phase_orig[0].header['NAXIS1'], phase_orig[0].header['NAXIS2'],
              phase_orig[0].header['NAXIS3']]
pyplot.figure(figsize=(6, 6))
pyplot.imshow(phase_orig[0].data[0, :, :], cmap='jet', aspect='equal')
phase_cen = fits.open(path.join(dir_airopa1, 'phase_maps', dir_year,
                                'cen.fits'))
pupil = fits.open(path.join(dir_airopa2, 'phase_maps', 'defocus',
                            ('pupil_' + pupil_shape + '.fits')))
pupil_data = pupil[0].data
pupil_size = pupil_data.shape
pyplot.figure(figsize=(6, 6))
pyplot.imshow(pupil_data, cmap='gray', aspect='equal')

# Calculate phases
phase_std = np.std(phase_orig[0].data[0, :, :])
print("Corner phase STD:  {0:.2f} nm".format(phase_std))
phase_mask = copy.deepcopy(phase_orig[0].data[0, :, :])
phase_mask[np.where(phase_mask)] = 1
phase_mask_x = 2 * (np.where(phase_mask)[1] - (phase_size[0] / 2)) / \
               phase_size[0]
phase_mask_y = 2 * (np.where(phase_mask)[0] - (phase_size[1] / 2)) / \
               phase_size[0]
pyplot.figure(figsize=(6, 6))
pyplot.imshow(phase_mask, cmap='gray', aspect='equal')
phase_one = copy.deepcopy(phase_mask)
phase_one[np.where(phase_mask)] = coeff * np.sqrt(3) * (2 * ((phase_mask_x ** 2)
                                                        + (phase_mask_y ** 2)) -
                                                        1)

for i_map in range(phase_size[2]):
    phase_orig[0].data[i_map, :, :] = phase_one

fig = pyplot.figure(figsize=(6, 6))
ax = pyplot.imshow(phase_one, cmap='jet', aspect='equal')
fig.colorbar(ax, label='OPD (nm)')
phase_cen[0].data[np.where(phase_cen[0].data)] = 0
std = coeff  # [nm]
pv = coeff * 2 * np.sqrt(3)  # [nm]
sr = np.exp(-(2 * np.pi * std * 1e-9 / lambd)**2)
print("Defocus PV:        {0:.2f} nm".format(pv))
print("Defocus STD:       {0:.2f} nm".format(std))
print("SR:                {0:.3f}".format(sr))

# Calculate expected PSF defocus
diam = 0
diam_aperture = 0

if pupil_shape == 'keck':
    diam = diams[0]
    diam_aperture = diam_apertures[0]
elif pupil_shape == 'dm':
    diam = diams[1]
    diam_aperture = diam_apertures[1]

fn_ff = f_ff / diam
p_scale_ff = 206265 / f_ff  # ["/m]
p_scale = ps / p_size  # ["/m]
fn = 206265 / (diam * p_scale)
fwhm_diffr1 = 1.02 * 206265 * lambd * 1e3 / diam  # [mas]
fwhm_diffr2 = fwhm_diffr1 / (ps * 1e3)  # [px]
fwhm_diffr3 = 1.02 * lambd * fn  # [m]
fwhm_diffr3_ff = fwhm_diffr1 / (p_scale_ff * 1e3)  # [m]
print('Diffraction FWHM:  {0:.3f} mas'.format(fwhm_diffr1))
print("Diffraction FWHM:  {0:.2f} px".format(fwhm_diffr2))
print("Front focus diffraction FWHM:  {0:.2f} um".format(fwhm_diffr3_ff * 1e6))
shift = 8 * fn_ff**2 * pv * 1e-9  # [m]
w0 = fwhm_diffr3_ff / (np.sqrt(2 * np.log(2)))  # [m]
zr = np.pi * (w0 ** 2) / lambd  # [m]
w = w0 * np.sqrt(1 + (shift / zr)**2)  # [m]
fwhm_focus1 = w * np.sqrt(2 * np.log(2))  # [m]
fwhm_focus2 = fwhm_focus1 * p_scale * 1e3  # [mas]
fwhm_focus2_ff = fwhm_focus1 * p_scale_ff * 1e3  # [mas]
fwhm_focus3 = fwhm_focus1 / p_size  # [px]
print('Defocus shift:     {0:.2f} mm'.format(shift * 1e3))
print('Defocus FWHM:      {0:.2f} mas'.format(fwhm_focus2))
print('Front focus defocus FWHM:      {0:.2f} mas'.format(fwhm_focus2_ff))
print("Defocus FWHM:      {0:.2f} px".format(fwhm_focus3))

# Calculate multiple expected PSF defocus
coeffs = np.arange(0, 510, 10)  # [nm]
pvs = coeffs * 2 * np.sqrt(3)  # [nm]
shifts = 8 * fn_ff**2 * pvs * 1e-9  # [m]
ws = w0 * np.sqrt(1 + (shifts / zr)**2)  # [m]
fwhm_focus1s = ws * np.sqrt(2 * np.log(2))  # [m]
fwhm_focus2s = fwhm_focus1s * p_scale_ff * 1e3  # [mas]

# Plot multiple expected PSF defocus
fig, ax1 = pyplot.subplots()
ax1.set_position([0.18, 0.15, 0.65, 0.65])
ax1.plot(coeffs, fwhm_focus2s, 'k-')
ax1.tick_params(labelsize=16)
ax1.set_xlabel('Defocus coefficient (nm)', fontsize=20)
ax1.set_ylabel('FWHM (mas)', color='k', fontsize=20)

# Simulate PSF
if fiber_illumination is not None:
    pupil_mesh_x, pupil_mesh_y = np.meshgrid(np.linspace(0, (pupil_size[1] - 1),
                                                         pupil_size[1]),
                                             np.linspace(0, (pupil_size[0] - 1),
                                                         pupil_size[0]))
    pupil_mesh_x -= (pupil_size[1] - 1) / 2
    pupil_mesh_y -= (pupil_size[0] - 1) / 2
    pupil_data = pupil_data * np.exp(-(((pupil_mesh_x**2) /
                                        (2 * fiber_illumination**2)) +
                                       ((pupil_mesh_y**2) /
                                        (2 * fiber_illumination**2))))
    pyplot.figure(figsize=(6, 6))
    pyplot.imshow(pupil_data, cmap='gray', aspect='equal')

ps_sim = diam_aperture * lambd * 206265 / (diam * n_side)
print('Pixel scale of simulated PSFs:     {0:.2f} mas/px'.format(ps_sim * 1e3))
aperture = np.pad(pupil_data, ((int(np.ceil((n_side - pupil_size[0]) / 2)),
                  int(np.floor((n_side - pupil_size[0]) / 2))),
                  (int(np.ceil((n_side - pupil_size[1]) / 2)),
                   int(np.floor((n_side - pupil_size[1]) / 2)))), 'constant')
pyplot.figure(figsize=(6, 6))
pyplot.imshow(aperture, cmap='gray', aspect='equal')
psf_sim = np.fft.fftshift(np.abs(np.fft.ifft2(np.fft.ifftshift(aperture))) ** 2)
psf_sim_peak = np.max(psf_sim)
pyplot.figure(figsize=(6, 6))
pyplot.imshow(psf_sim, cmap='jet', aspect='equal',
              norm=colors.LogNorm(
                  vmin=(psf_sim.max() * 1e-4), vmax=psf_sim.max()))
pyplot.xlim([((n_side / 2) - psf_buffer), ((n_side / 2) + psf_buffer)])
pyplot.ylim([((n_side / 2) - psf_buffer), ((n_side / 2) + psf_buffer)])
fit_params_psf_diff = gauss_fit(psf_sim)
fwhm_psf_diff = 2 * np.sqrt(2 * np.log(2)) * np.mean(fit_params_psf_diff[3:]) \
                * ps_sim
print("Fitted FWHM of the diffraction limited PSF: {0:.2f} mas".format(
    fwhm_psf_diff * 1e3))
x_space = np.arange(pupil_size[1])
y_space = np.arange(pupil_size[0])
x_grid, y_grid = np.meshgrid(x_space, y_space)
diam_px = np.where(np.any(pupil_data, axis=0))[0][-1] - \
          np.where(np.any(pupil_data, axis=0))[0][0]
phase_sim = coeff * np.sqrt(3) * (2 * ((((x_grid - ((pupil_size[1] - 1) / 2)) /
                                         (diam_px / 2)) ** 2) +
                                       (((y_grid - ((pupil_size[0] - 1) / 2)) /
                                         (diam_px / 2)) ** 2)) - 1)
pyplot.figure(figsize=(6, 6))
pyplot.imshow(phase_sim, cmap='jet', aspect='equal')
phase_sim_complex = np.array([[complex(0, (2 * np.pi * 1e-9 * phase_sim[j, i] /
                             lambd)) for i in range(pupil_size[1])] for j in
                             range(pupil_size[0])])
pupil_complex = pupil_data * np.exp(phase_sim_complex)
aperture_complex = np.pad(pupil_complex, ((int(np.ceil((n_side - pupil_size[0])
                          / 2)), int(np.floor((n_side - pupil_size[0]) / 2))),
                          (int(np.ceil((n_side - pupil_size[1]) / 2)),
                           int(np.floor((n_side - pupil_size[1]) / 2)))),
                          'constant')
psf_sim_complex = np.fft.fftshift(np.abs(np.fft.ifft2(np.fft.ifftshift(
    aperture_complex))) ** 2)
psf_sim_complex_peak = np.max(psf_sim_complex)
pyplot.figure(figsize=(6, 6))
pyplot.imshow(psf_sim_complex, cmap='jet', aspect='equal',
              norm=colors.LogNorm(
                  vmin=(psf_sim.max() * 1e-4), vmax=psf_sim.max()))
pyplot.xlim([((n_side / 2) - psf_buffer), ((n_side / 2) + psf_buffer)])
pyplot.ylim([((n_side / 2) - psf_buffer), ((n_side / 2) + psf_buffer)])
sim_sr = psf_sim_complex_peak / psf_sim_peak
print("Fitted SR of the defocused PSF: {0:.2f}".format(sim_sr))
x_grid_sim, y_grid_sim = np.meshgrid(np.arange(-(n_side / 2), (n_side / 2), 1),
                                     np.arange(-(n_side / 2), (n_side / 2), 1))
psf_sim_complex_rad = psf_sim_complex.flatten()
sim_rad = np.hypot(x_grid_sim, y_grid_sim).flatten()
pyplot.figure(figsize=(6, 6))
pyplot.scatter(sim_rad, psf_sim_complex_rad, color='k')
pyplot.xlim([0, 30])
pyplot.ylim([0, (psf_sim_complex_peak * 1.3)])
idx_fit = np.where((sim_rad >
                    (sim_rad[np.where(psf_sim_complex_rad ==
                                      psf_sim_complex_peak)[0][0]] - 2)) &
                   (sim_rad <
                    (sim_rad[np.where(psf_sim_complex_rad ==
                                      psf_sim_complex_peak)[0][0]] + 2)))[0]
coeff1d, _ = optimize.curve_fit(gauss1d, sim_rad[idx_fit], psf_sim_complex_rad[idx_fit])
sim_fwhm = 2 * (coeff1d[1] + (np.sqrt(2 * np.log(2)) * coeff1d[2])) * ps_sim
print("Fitted FWHM of the defocused PSF: {0:.2f} mas".format(sim_fwhm * 1e3))

# Save files
if not path.exists(path.join(dir_airopa2, 'phase_maps', 'defocus')):
    makedirs(path.join(dir_airopa2, 'phase_maps', 'defocus'))

phase_orig.writeto(path.join(dir_airopa2, 'phase_maps', 'defocus',
                             'phase_maps_reg_defocus.fits'), overwrite=True)
phase_cen.writeto(path.join(dir_airopa2, 'phase_maps', 'defocus',
                            'cen_defocus.fits'), overwrite=True)

# Copy files
for i_file in ['x_reg', 'y_reg']:
    shutil.copyfile(path.join(dir_airopa1, 'phase_maps', dir_year,
                              (i_file + '.fits')),
                    path.join(dir_airopa2, 'phase_maps', 'defocus',
                              (i_file + '.fits')))

# End program
phase_orig.close()
phase_cen.close()
pyplot.show()
print('Program terminated')
