"""Analyze the grid of PSFs generated by AIROPA.

The instrumental grid is saved from the "psf1" attribute of the output structure
from the "inst_otf" program in IDL. The instrumental+atmospheric grid is output
by "find_stf" using "debug=1". All these files were generated analyzing the
"c2006.fits" image taken on 2017-08-23.

Data parameters:
    dir_test (str) - Test data folder
    img_test (str) - Name of the instrumental PSF grid
"""

from os import environ, path

from astropy.io import fits
from matplotlib import colors, pyplot
import numpy as np


# Data parameters
dir_test = ('/g/lu/scratch/jlu/work/ao/airopa/AIROPA_TEST/airopa_benchmarks/' +
            'psf_grids')
img_instr = 'instr'
img_atm = 'instr_atm'

# Graphic parameters
side_plot = 7
side_psf = 40
scale_min = 0.00003
scale_max = 0.02

# Load input data
instr = fits.open(path.join(dir_test, (img_instr + '.fits')))
side_instr = int(np.sqrt(instr[0].data.shape[0]))
instr_psf_side = instr[0].data[0, :, :].shape[0]
instr_psf_center = int((instr_psf_side - 1) / 2) + 1
atm = fits.open(path.join(dir_test, (img_atm + '.fits')))
side_atm = int(np.sqrt(atm[0].data.shape[0]))
atm_psf_side = atm[0].data[0, :, :].shape[0]
atm_psf_center = int((atm_psf_side - 1) / 2) + 1

# Show grids
fig_inst = pyplot.figure(figsize=(7, 7))

for i_tile in range(side_plot ** 2):
    row_tile = int(np.floor(i_tile / side_plot))
    col_tile = (i_tile % side_plot)
    i_tile_instr = (round((row_tile * (side_instr - 1) / (side_plot - 1))) *
                    side_instr) + round((col_tile * (side_instr - 1) /
                                         (side_plot - 1)))
    instr_trim = instr[0].data[i_tile_instr, (instr_psf_center - side_psf):
                               (instr_psf_center + side_psf), (instr_psf_center
                               - side_psf): (instr_psf_center + side_psf)]
    ax = pyplot.subplot(side_plot, side_plot, (i_tile + 1))
    pyplot.imshow(instr_trim, cmap='jet', aspect='equal',
                  norm=colors.LogNorm(vmin=scale_min, vmax=scale_max))
    pyplot.xticks([])
    pyplot.yticks([])

fig_inst.tight_layout()
fig_atm = pyplot.figure(figsize=(7, 7))

for i_tile in range(side_plot ** 2):
    row_tile = int(np.floor(i_tile / side_plot))
    col_tile = (i_tile % side_plot)
    i_tile_atm = (round((row_tile * (side_atm - 1) / (side_plot - 1))) *
                  side_atm) + round((col_tile * (side_atm - 1) / (side_plot -
                                                                  1)))
    atm_trim = atm[0].data[i_tile_atm, (atm_psf_center - side_psf):
                           (atm_psf_center + side_psf), (atm_psf_center -
                           side_psf): (atm_psf_center + side_psf)]
    ax = pyplot.subplot(side_plot, side_plot, (i_tile + 1))
    pyplot.imshow(atm_trim, cmap='jet', aspect='equal',
                  norm=colors.LogNorm(vmin=scale_min, vmax=scale_max))
    pyplot.xticks([])
    pyplot.yticks([])

fig_atm.tight_layout()
pyplot.show()
print()
