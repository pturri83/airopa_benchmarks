"""Calculate the coordinates in pixels of the T/T star in the GC images.

Data parameters:
    psf_pix (float [2]) - Coordinate of the PSF star in the image [px]
    psf_sec (float [2]) - Coordinate of the PSF star in 'psf_stars.dat' ["]
"""

import numpy as np


# Data parameters
psf_pix = [510, 758]
psf_sec = [0.053, 1.210]

# Fixed parameters
tt_ra = [17, 45, 40.72]  # T/T star (USNO-A2.0 0600.28577051) right ascension
tt_dec = [-29, 0, 11.2]  # T/T star (USNO-A2.0 0600.28577051) declination
sgra_ra = [17, 45, 40.036]  # SgrA* right ascension
sgra_dec = [-29, 0, 28.17]  # SgrA* declination
px_s = 0.009952  # NIRC2 pixel scale [px/"]

# Convert coordinates
tt_dec_2 = tt_dec[0] + (np.sign(tt_dec[0]) * tt_dec[1] / 60) + \
           (np.sign(tt_dec[0]) * tt_dec[2] / 3600)
tt_dec_3 = tt_dec_2 * 3600
tt_ra_2 = [(i * 15) for i in tt_ra]
tt_ra_3 = (tt_ra_2[0] * 3600) + (tt_ra_2[1] * 60) + tt_ra_2[2]
sgra_dec_2 = sgra_dec[0] + (np.sign(sgra_dec[0]) * sgra_dec[1] / 60) + \
           (np.sign(sgra_dec[0]) * sgra_dec[2] / 3600)
sgra_dec_3 = sgra_dec_2 * 3600
sgra_ra_2 = [(i * 15) for i in sgra_ra]
sgra_ra_3 = (sgra_ra_2[0] * 3600) + (sgra_ra_2[1] * 60) + sgra_ra_2[2]

# Calculate distances
delta_ra = (tt_ra_3 - sgra_ra_3) * np.cos(np.deg2rad(tt_dec_2))
delta_dec = tt_dec_3 - sgra_dec_3
delta_x_sgra_tt = -delta_ra / px_s
delta_y_sgra_tt = delta_dec / px_s
delta_x_sgra_psf = -psf_sec[0] / px_s
delta_y_sgra_psf = psf_sec[1] / px_s
x_tt = delta_x_sgra_tt + psf_pix[0]
y_tt = delta_y_sgra_tt + psf_pix[1]

# Show results
print()
print('-----------------------------------------------------------------------')
print(' T/T star x coordinate: {0}'.format(int(x_tt)))
print(' T/T star y coordinate: {0}'.format(int(y_tt)))
print('-----------------------------------------------------------------------')
print()
