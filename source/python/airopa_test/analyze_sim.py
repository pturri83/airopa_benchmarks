from datetime import datetime
from glob import glob
from os import makedirs, path
import shutil
import time

from astropy import stats, table
from astropy.io import fits
from matplotlib import pyplot, colors
import numpy
from scipy import optimize

from flystar import match


def analyze_sim(dir_test, scene_type, dr_tol, dm_tol, bright_mag,
                img_orig_range, img_res_range, img_res_range_2, test_pos,
                test_r, quiv_scale, quiv_scale_2, quiv_scale_bright,
                quiv_legend, quiv_legend_2, dr_range, dr_range_2,
                dr_range_grid_phase, dr_zoom, dr_histmax, dr_histmax_2,
                dr_histbin, dr_histbin_2, m_range, m_range_color, dr_max_bright,
                dm_histmax, dm_histmax_2, dm_histbin, dm_histbin_2, dm_range,
                dm_range_2, dm_range_bright, dm_range_grid_phase,
                missed_histmax, missed_histbin, fake_histmax, fake_histbin,
                m_range_fake, m_range_fake_color, fvu_range, fvu_range_color,
                fvu_range_fake, dr_fvu_min, dm_fvu_min, m_fvu, px_phase,
                psf_load, m_histmax, m_histbin):

    # Start program
    print("\nAnalysis started")

    # Save the parameters
    folder_out = path.join(dir_test, 'plot')

    if path.isdir(folder_out):

        for i in range(5):

            try:
                shutil.rmtree(folder_out)
                time.sleep(2)
            except:
                time.sleep(2)

    makedirs(folder_out)
    now = datetime.now()
    params = open(path.join(dir_test, 'plot', 'params_plt.txt'), 'w')
    params.write('Date analyzed:            ' + str(now.year) + '-' +
                 str(now.month) + '-' + str(now.day) + '\n\n')
    params.write('--- PARAMETERS ---' + '\n')
    params.write('Matching distance (px):   ' + str(dr_tol) + '\n')
    params.write('Matching magnitude:       ' + str(dm_tol) + '\n')

    if scene_type != 'grid':
        params.write('Bright magnitude:         ' + str(bright_mag) + '\n')

    params.close()

    # Fixed parameters
    modes_str = ['legacy', 'single', 'variable']  # Name of AIROPA modes
    xy_buffer = 100  # Buffer in x-y images (px)
    img_ps = 0.009942  # Pixel scale

    # Load input data
    tab_in = table.Table.read(path.join(dir_test, 'image', 'img_sim.cat'),
                              format='ascii', delimiter='\s')
    tab_in.rename_column('xpix', 'x')
    tab_in.rename_column('ypix', 'y')
    tab_in.rename_column('mags', 'm')

    img_orig = fits.open(path.join(dir_test, 'image', 'img_sim.fits'))
    img_size = [img_orig[0].header['NAXIS1'], img_orig[0].header['NAXIS2']]

    # Find test stars
    for i_test in range(8):
        test_dist = numpy.hypot((tab_in['x'] - test_pos[i_test][0]),
                                (tab_in['y'] - test_pos[i_test][1]))
        test_idx = numpy.argmin(test_dist)
        test_pos[i_test] = [int(tab_in['x'][test_idx]),
                            int(tab_in['y'][test_idx])]

    # Show original image
    img_ticks = 2
    img_shape = img_orig[0].data.shape
    xy_px = numpy.array(img_shape)
    xy_ticks_max = numpy.floor((xy_px * img_ps) / (2 * img_ticks)) * img_ticks
    x_ticks = numpy.arange(-xy_ticks_max[1], (xy_ticks_max[1] + img_ticks), img_ticks)
    y_ticks = numpy.arange(-xy_ticks_max[0], (xy_ticks_max[0] + img_ticks), img_ticks)
    x_ticks_orig = (x_ticks / img_ps) + ((xy_px[1] - 1) / 2)
    y_ticks_orig = (y_ticks / img_ps) + ((xy_px[0] - 1) / 2)
    x_ticks_str = [str(i) for i in numpy.around(x_ticks, decimals=10)]
    y_ticks_str = [str(i) for i in numpy.around(y_ticks, decimals=10)]
    fig = pyplot.figure(figsize=(6, 6))
    ax = fig.add_subplot(111)
    pyplot.imshow(img_orig[0].data, clim=img_orig_range, cmap='hot',
                  aspect='equal')
    ax.set_position([0.25, 0.15, 0.65, 0.65])
    ax.invert_yaxis()
    ax.set_xticks(x_ticks_orig)
    ax.set_yticks(y_ticks_orig)
    ax.set_xticklabels(x_ticks_str)
    ax.set_yticklabels(y_ticks_str)
    pyplot.xlabel("x (\")", fontsize=20)
    pyplot.ylabel("y (\")", fontsize=20)
    pyplot.tick_params(labelsize=20)
    save_path = path.join(folder_out, 'image')
    pyplot.savefig(save_path + '.png')
    pyplot.savefig(save_path + '.pdf')
    pyplot.close(fig)

    # Histogram of stars by magnitude
    fig = pyplot.figure(figsize=(6, 6.5))
    ax = fig.add_subplot(111)
    ax.set_position([0.25, 0.15, 0.65, 0.65])
    pyplot.hist(tab_in.columns['m'],
                bins=numpy.arange(m_range[0], m_range[1], m_histbin), color='b')
    pyplot.xlim(m_range)
    pyplot.ylim([0, m_histmax])
    pyplot.xlabel("m", fontsize=20)
    pyplot.ylabel("N", fontsize=20)
    pyplot.tick_params(labelsize=20)
    save_path = path.join(folder_out, 'm_orig')
    pyplot.savefig(save_path + '.png')
    pyplot.savefig(save_path + '.pdf')
    pyplot.close(fig)

    # Histogram of stars by magnitude with text
    fig = pyplot.figure(figsize=(6, 6.5))
    ax = fig.add_subplot(111)
    ax.set_position([0.25, 0.15, 0.65, 0.65])
    pyplot.hist(tab_in.columns['m'],
                bins=numpy.arange(m_range[0], m_range[1], m_histbin), color='b')
    pyplot.text((m_range[0] + 0.2), (3 * m_histmax / 4),
                (r"$N_{input}$=" + " {0:d}".format(len(tab_in.columns['m']))),
                ha='left', fontsize=20)
    pyplot.xlim(m_range)
    pyplot.ylim([0, m_histmax])
    pyplot.xlabel("m", fontsize=20)
    pyplot.ylabel("N", fontsize=20)
    pyplot.tick_params(labelsize=20)
    save_path = path.join(folder_out, 'm_orig_text')
    pyplot.savefig(save_path + '.png')
    pyplot.savefig(save_path + '.pdf')
    pyplot.close(fig)

    # Show original test stars
    test_str = ['tl1', 'tl2', 'tr1', 'tr2', 'bl1', 'bl2', 'br1', 'br2']
    img_ticks_zoom = 0.2
    xy_ticks_max_zoom = numpy.floor((xy_px * img_ps) / (2 * img_ticks_zoom)) * img_ticks
    x_ticks_zoom = numpy.arange(-xy_ticks_max_zoom[1], (xy_ticks_max_zoom[1] + img_ticks_zoom), img_ticks_zoom)
    y_ticks_zoom = numpy.arange(-xy_ticks_max_zoom[0], (xy_ticks_max_zoom[0] + img_ticks_zoom), img_ticks_zoom)
    x_ticks_orig_zoom = (x_ticks_zoom / img_ps) + ((xy_px[1] - 1) / 2)
    y_ticks_orig_zoom = (y_ticks_zoom / img_ps) + ((xy_px[0] - 1) / 2)
    x_ticks_str_zoom = [str(i) for i in numpy.around(x_ticks_zoom, decimals=10)]
    y_ticks_str_zoom = [str(i) for i in numpy.around(y_ticks_zoom, decimals=10)]

    for i_test in range(8):
        fig = pyplot.figure(figsize=(6, 6))
        ax = fig.add_subplot(111)
        pyplot.imshow(img_orig[0].data, clim=img_orig_range, cmap='hot',
                      aspect='equal')
        ax.set_xticks(x_ticks_orig_zoom)
        ax.set_yticks(y_ticks_orig_zoom)
        ax.set_xticklabels(x_ticks_str_zoom)
        ax.set_yticklabels(y_ticks_str_zoom)
        pyplot.scatter((tab_in['x'] - 1), (tab_in['y'] - 1), marker='o', c='g',
                       s=70)
        pyplot.axis([(test_pos[i_test][0] - test_r), (test_pos[i_test][0] +
                    test_r), (test_pos[i_test][1] - test_r),
                    (test_pos[i_test][1] + test_r)])
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.xlabel("x (\")", fontsize=20)
        pyplot.ylabel("y (\")", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, ('image_test_' + test_str[i_test]))
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

    # Analyze AIROPA modes
    if psf_load:
        mode_range = range(1, 2)
    else:
        mode_range = range(0, 3)

    r_max = (numpy.hypot(img_size[0], img_size[1]) / 2) * img_ps
    r_ticks = numpy.arange(0, r_max, numpy.around((r_max / 5), decimals=0))
    r_ticks_orig = r_ticks / img_ps

    for i_mode in mode_range:

        # Load output data
        print("\rAnalyzing AIROPA \"{0}\" mode...".format(modes_str[i_mode]),
              end="")
        folder_in = path.join(dir_test, 'fit', modes_str[i_mode])
        img_res = fits.open(path.join(folder_in, 'img_sim_res.fits'))
        cat_out1 = glob(path.join(folder_in, 'img_sim_*_stf.lis'))
        tab_out = table.Table.read(cat_out1[0], format='ascii', delimiter='\s')
        tab_out.rename_column('col4', 'x')
        tab_out.rename_column('col5', 'y')
        tab_out.rename_column('col2', 'm')
        cat_out2 = glob(path.join(folder_in, 'img_sim_*_metrics.txt'))
        metrics_out = table.Table.read(cat_out2[0], format='ascii',
                                       data_start=0)
        tab_out.add_column(table.Column(metrics_out[metrics_out.colnames[0]] **
                           2), name='fvu')
        folder_img = path.join(dir_test, 'image')
        psf_grid = None

        if i_mode == 2:
            psf_grid_tmp = fits.open(path.join(folder_img, 'grid_pos.fits'))
            psf_grid = psf_grid_tmp[0].data

        # Match stars
        x_in = tab_in.columns['x']
        y_in = tab_in.columns['y']
        m_in = tab_in.columns['m']
        x_out = tab_out.columns['x']
        y_out = tab_out.columns['y']
        m_out = tab_out.columns['m']
        idx_out, idx_in, _, dm = match.match(x_out, y_out, m_out, x_in, y_in,
                                             m_in, dr_tol=dr_tol, dm_tol=dm_tol,
                                             verbose=False)
        x = x_in[idx_in]
        n_stars = len(x)
        y = y_in[idx_in]
        m = m_in[idx_in]
        fvu = tab_out.columns['fvu'][idx_out]
        idx_missed = numpy.where(~numpy.isin(numpy.arange(len(tab_in)),
                                 idx_in))[0]
        tab_in_missed = tab_in[idx_missed]
        x_missed = tab_in_missed.columns['x']
        y_missed = tab_in_missed.columns['y']
        idx_missed_in = numpy.where((x_missed >= 0) & (x_missed <= 1023) &
                                    (y_missed >= 0) & (y_missed <= 1023))[0]
        tab_in_missed = tab_in_missed[idx_missed_in]
        x_missed = tab_in_missed.columns['x']
        y_missed = tab_in_missed.columns['y']
        m_missed = tab_in_missed.columns['m']
        idx_fake = numpy.where(~numpy.isin(numpy.arange(len(tab_out)),
                               idx_out))[0]
        tab_out_fake = tab_out[idx_fake]
        x_fake = tab_out_fake.columns['x']
        y_fake = tab_out_fake.columns['y']
        m_fake = tab_out_fake.columns['m']
        fvu_fake = tab_out_fake.columns['fvu']

        # Analyze matched stars
        dm = dm - numpy.mean(dm)
        dx = tab_out.columns['x'][idx_out] - x
        dx = dx - numpy.mean(dx)
        dy = tab_out.columns['y'][idx_out] - y
        dy = dy - numpy.mean(dy)
        r = numpy.hypot((x - (img_size[0] / 2)), (y - (img_size[1] / 2)))
        dr = numpy.hypot(dx, dy)
        dr_filt = dr
        dm_filt = dm

        # if scene_type == 'gc':
        #     dr_filt = stats.sigma_clip(dr, sigma=3, maxiters=5)
        #     dm_filt = stats.sigma_clip(dm, sigma=3, maxiters=5)
        #     fvu_filt = stats.sigma_clip(fvu, sigma=3, maxiters=5)
        dr_filt = stats.sigma_clip(dr, sigma=3, maxiters=5)
        dm_filt = stats.sigma_clip(dm, sigma=3, maxiters=5)
        fvu_filt = stats.sigma_clip(fvu, sigma=3, maxiters=5)

        dr_mean_all = numpy.mean(dr_filt)
        dm_mean_all = numpy.mean(numpy.abs(dm_filt))
        fvu_mean_all = numpy.mean(fvu_filt)

        # Match bright stars
        dm_bright = []
        dx_bright = []
        dy_bright = []
        dr_bright = []
        tab_in_bright = []
        idx_in_bright = []
        idx_out_bright = []

        if scene_type != 'grid':
            tab_in_bright = tab_in[numpy.where(tab_in.columns['m'] <
                                               bright_mag)]
            idx_out_bright, idx_in_bright, _, dm_bright = match.match(
                tab_out.columns['x'], tab_out.columns['y'],
                tab_out.columns['m'], tab_in_bright.columns['x'],
                tab_in_bright.columns['y'], tab_in_bright.columns['m'],
                dr_tol=dr_tol, dm_tol=dm_tol, verbose=False)

        # Analyze matched bright stars
        dr_mean_bright = None
        dm_mean_bright = None
        fvu_mean_bright = None

        if scene_type != 'grid':
            dm_bright = numpy.abs(dm_bright - numpy.mean(dm_bright))
            dx_bright = (tab_out.columns['x'][idx_out_bright] -
                         tab_in_bright.columns['x'][idx_in_bright])
            dx_bright = dx_bright - numpy.mean(dx_bright)
            dy_bright = (tab_out.columns['y'][idx_out_bright] -
                         tab_in_bright.columns['y'][idx_in_bright])
            dy_bright = dy_bright - numpy.mean(dy_bright)
            dr_bright = numpy.hypot(dx_bright, dy_bright)
            fvu_bright = tab_out.columns['fvu'][idx_out_bright]
            dr_bright_filt = stats.sigma_clip(dr_bright, sigma=3, maxiters=5)
            dr_mean_bright = numpy.mean(dr_bright_filt)
            dm_bright_filt = stats.sigma_clip(dm_bright, sigma=3, maxiters=5)
            dm_mean_bright = numpy.mean(dm_bright_filt)
            fvu_bright_filt = stats.sigma_clip(fvu_bright, sigma=3, maxiters=5)
            fvu_mean_bright = numpy.mean(fvu_bright_filt)

        # Analyze FVU
        m_fit = (m <= m_fvu)
        fvu_fit = fvu[m_fit]
        dr_fit = dr[m_fit]
        fit_dr_fvu = numpy.polyfit(numpy.log10(dr_fit), numpy.log10(fvu_fit), 1)

        # Analyze pixel phase
        x_phase = None
        y_phase = None
        x_phase_fit = None
        y_phase_fit = None
        x_phase_res_nofit = None
        y_phase_res_nofit = None
        x_phase_res_fit = None
        y_phase_res_fit = None

        if px_phase:
            x_phase = x - numpy.floor(x)
            y_phase = y - numpy.floor(y)
            # noinspection PyTypeChecker
            x_phase_fit, _ = optimize.curve_fit(fit_sine, x_phase, dx,
                                                p0=[0.01, 1, 0, 0])
            # noinspection PyTypeChecker
            y_phase_fit, _ = optimize.curve_fit(fit_sine, y_phase, dy,
                                                p0=[0.01, 1, 0, 0])
            x_phase_res_nofit = numpy.mean(numpy.abs(dx))
            y_phase_res_nofit = numpy.mean(numpy.abs(dy))
            x_phase_res_fit = numpy.std(numpy.abs(dx - fit_sine(x_phase,
                                        x_phase_fit[0], x_phase_fit[1],
                                        x_phase_fit[2], x_phase_fit[3])))
            y_phase_res_fit = numpy.std(numpy.abs(dy - fit_sine(y_phase,
                                        y_phase_fit[0], y_phase_fit[1],
                                        y_phase_fit[2], y_phase_fit[3])))

        # Analyze grid phase
        near_phase = None

        if i_mode == 2:
            near_phase_idx = \
                numpy.argmin([numpy.hypot((tab_out.columns['x'][idx_out][i] -
                                           psf_grid[:, 0]),
                                          (tab_out.columns['y'][idx_out][i] -
                                           psf_grid[:, 1])) for i in
                              range(n_stars)], axis=1)
            near_phase = numpy.hypot((tab_out.columns['x'][idx_out] -
                                      psf_grid[near_phase_idx, 0]),
                                     (tab_out.columns['y'][idx_out] -
                                      psf_grid[near_phase_idx, 1]))

        # Show residual image
        folder_out = path.join(dir_test, 'plot', modes_str[i_mode])

        if not path.exists(folder_out):
            makedirs(folder_out)

        fig = pyplot.figure(figsize=(6, 6))
        ax = fig.add_subplot(111)
        pyplot.imshow(img_res[0].data, clim=img_res_range, cmap='hot',
                      aspect='equal')
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        ax.invert_yaxis()
        ax.set_xticks(x_ticks_orig)
        ax.set_yticks(y_ticks_orig)
        ax.set_xticklabels(x_ticks_str)
        ax.set_yticklabels(y_ticks_str)
        pyplot.xlabel("x (\")", fontsize=20)
        pyplot.ylabel("y (\")", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'image_res')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Show second residual image
        if img_res_range_2:
            fig = pyplot.figure(figsize=(6, 6))
            ax = fig.add_subplot(111)
            pyplot.imshow(img_res[0].data, clim=img_res_range_2, cmap='hot',
                          aspect='equal')
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            ax.invert_yaxis()
            ax.set_xticks(x_ticks_orig)
            ax.set_yticks(y_ticks_orig)
            ax.set_xticklabels(x_ticks_str)
            ax.set_yticklabels(y_ticks_str)
            pyplot.xlabel("x (\")", fontsize=20)
            pyplot.ylabel("y (\")", fontsize=20)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'image_res_2')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Show residual test stars
        for i_test in range(8):
            fig = pyplot.figure(figsize=(6, 6))
            ax = fig.add_subplot(111)
            pyplot.imshow(img_res[0].data, clim=img_res_range, cmap='hot',
                          aspect='equal')
            ax.set_xticks(x_ticks_orig_zoom)
            ax.set_yticks(y_ticks_orig_zoom)
            ax.set_xticklabels(x_ticks_str_zoom)
            ax.set_yticklabels(y_ticks_str_zoom)
            pyplot.scatter((tab_in['x'] - 1), (tab_in['y'] - 1), marker='o',
                           c='g', s=70)
            pyplot.axis([(test_pos[i_test][0] - test_r), (test_pos[i_test][0] +
                        test_r), (test_pos[i_test][1] - test_r),
                        (test_pos[i_test][1] + test_r)])
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.xlabel("x (\")", fontsize=20)
            pyplot.ylabel("y (\")", fontsize=20)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, ('image_res_test_' +
                                  test_str[i_test]))
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Show second residual test stars
        if img_res_range_2:
            for i_test in range(8):
                fig = pyplot.figure(figsize=(6, 6))
                ax = fig.add_subplot(111)
                pyplot.imshow(img_res[0].data, clim=img_res_range, cmap='hot',
                              aspect='equal')
                ax.set_xticks(x_ticks_orig_zoom)
                ax.set_yticks(y_ticks_orig_zoom)
                ax.set_xticklabels(x_ticks_str_zoom)
                ax.set_yticklabels(y_ticks_str_zoom)
                pyplot.scatter((tab_in['x'] - 1), (tab_in['y'] - 1), marker='o',
                               c='g', s=70)
                pyplot.axis([(test_pos[i_test][0] - test_r),
                             (test_pos[i_test][0] + test_r),
                             (test_pos[i_test][1] - test_r),
                             (test_pos[i_test][1] + test_r)])
                ax.set_position([0.25, 0.15, 0.65, 0.65])
                pyplot.xlabel("x (\")", fontsize=20)
                pyplot.ylabel("y (\")", fontsize=20)
                pyplot.tick_params(labelsize=20)
                save_path = path.join(folder_out, ('image_res_test_' +
                                      test_str[i_test] + '_2'))
                pyplot.savefig(save_path + '.png')
                pyplot.savefig(save_path + '.pdf')
                pyplot.close(fig)

        # Position residual scatter plot
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.scatter(dx, dy, s=5, c='k')

        # Temp stuff, to use arcsec instead of px as axis units
        ticks_1 = numpy.arange(-15, 15, 5)
        ticks_str_1 = [str(i) for i in numpy.around(ticks_1, decimals=10)]
        ticks_orig_1 = ticks_1 / img_ps / 1e3
        ax.set_xticks(ticks_orig_1)
        ax.set_yticks(ticks_orig_1)
        ax.set_xticklabels(ticks_str_1)
        ax.set_yticklabels(ticks_str_1)
        
        pyplot.xlim([-dr_range, dr_range])
        pyplot.ylim([-dr_range, dr_range])
        ax.set_aspect('equal', 'box')
        pyplot.xlabel(r"$\Delta$x (mas)", fontsize=20)
        pyplot.ylabel(r"$\Delta$y (mas)", fontsize=20, labelpad=-2)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'dxdy')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Second position residual scatter plot
        if dr_range_2:
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.scatter(dx, dy, s=5, c='k')

            # Temp stuff, to use arcsec instead of px as axis units
            ticks_1 = numpy.arange(-0.03, 0.03, 0.01)
            ticks_str_1 = [str(i) for i in numpy.around(ticks_1, decimals=10)]
            ticks_orig_1 = ticks_1 / img_ps / 1e3
            ax.set_xticks(ticks_orig_1)
            ax.set_yticks(ticks_orig_1)
            ax.set_xticklabels(ticks_str_1)
            ax.set_yticklabels(ticks_str_1)

            pyplot.xlim([-dr_range_2, dr_range_2])
            pyplot.ylim([-dr_range_2, dr_range_2])
            ax.set_aspect('equal', 'box')
            pyplot.xlabel(r"$\Delta$x (mas)", fontsize=20)
            pyplot.ylabel(r"$\Delta$y (mas)", fontsize=20, labelpad=-2)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'dxdy_2')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Position residual scatter plot in color
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(dx, dy, s=5, c=m,
                                 cmap=pyplot.get_cmap('plasma'),
                                 vmin=m_range_color[0], vmax=m_range_color[1])
        pyplot.xlim([-dr_range, dr_range])
        pyplot.ylim([-dr_range, dr_range])
        ax.set_aspect('equal', 'box')
        pyplot.xlabel(r"$\Delta$x (px)", fontsize=20)
        pyplot.ylabel(r"$\Delta$y (px)", fontsize=20, labelpad=-2)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("m", fontsize=20)
        save_path = path.join(folder_out, 'dxdy_color')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Position residual scatter plot, zoom in
        if scene_type != 'grid':
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.scatter(dx, dy, s=5, c='k')
            pyplot.xlim([-dr_zoom, dr_zoom])
            pyplot.ylim([-dr_zoom, dr_zoom])
            ax.set_aspect('equal', 'box')
            pyplot.xlabel(r"$\Delta$x (px)", fontsize=20)
            pyplot.ylabel(r"$\Delta$y (px)", fontsize=20, labelpad=-2)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'dxdy_zoom')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Position residual scatter plot in color, zoom in
        if scene_type != 'grid':
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            scatter = pyplot.scatter(dx, dy, s=5, c=m,
                                     cmap=pyplot.get_cmap('plasma'),
                                     vmin=m_range_color[0],
                                     vmax=m_range_color[1])
            pyplot.xlim([-dr_zoom, dr_zoom])
            pyplot.ylim([-dr_zoom, dr_zoom])
            ax.set_aspect('equal', 'box')
            pyplot.xlabel(r"$\Delta$x (px)", fontsize=20)
            pyplot.ylabel(r"$\Delta$y (px)", fontsize=20, labelpad=-2)
            pyplot.tick_params(labelsize=20)
            colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
            fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
            colorbar_ax.xaxis.set_ticks_position('top')
            colorbar_ax.tick_params(labelsize=20)
            colorbar_ax.set_xlabel("m", fontsize=20)
            save_path = path.join(folder_out, 'dxdy_color_zoom')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Quiver plot
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        # quiv = pyplot.quiver(x, y, dx, dy, scale=quiv_scale,
        #                      scale_units='height', width=0.01)
        # pyplot.quiverkey(quiv, 0.4, 0.9, quiv_legend, (str(quiv_legend) +
        #                  " px"), labelpos='W', coordinates='figure',
        #                  fontproperties={'size': 20})
        quiv = pyplot.quiver(x, y, dx * img_ps, dy * img_ps, scale=quiv_scale,
                             scale_units='height', width=0.01)
        pyplot.quiverkey(quiv, 0.4, 0.9, quiv_legend / 1e3, (str(quiv_legend)
                                                               + "mas"), labelpos='W', coordinates='figure',
                         fontproperties={'size': 20})

        # Temp stuff, to use arcsec instead of px as axis units
        ticks_1 = numpy.arange(-8, 8, 2)
        ticks_str_1 = [str(i) for i in numpy.around(ticks_1, decimals=10)]
        ticks_orig_1 = (ticks_1 / img_ps) + (img_shape[0] / 2)
        ax.set_xticks(ticks_orig_1)
        ax.set_yticks(ticks_orig_1)
        ax.set_xticklabels(ticks_str_1)
        ax.set_yticklabels(ticks_str_1)
        
        pyplot.xlim([-xy_buffer, (img_size[0] + xy_buffer)])
        pyplot.ylim([-xy_buffer, (img_size[1] + xy_buffer)])
        ax.set_aspect('equal', 'box')
        pyplot.xlabel("x (\")", fontsize=20)
        pyplot.ylabel("y (\")", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'xydxdy')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Second quiver plot
        if quiv_scale_2 and quiv_legend_2:
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            quiv = pyplot.quiver(x, y, dx * img_ps, dy * img_ps, scale=quiv_scale_2,
                                 scale_units='height', width=0.01)
            pyplot.quiverkey(quiv, 0.4, 0.9, quiv_legend_2 / 1e3, (str(quiv_legend_2)
                             + "mas"), labelpos='W', coordinates='figure',
                             fontproperties={'size': 20})

            # Temp stuff, to use arcsec instead of px as axis units
            ax.set_xticks(ticks_orig_1)
            ax.set_yticks(ticks_orig_1)
            ax.set_xticklabels(ticks_str_1)
            ax.set_yticklabels(ticks_str_1)
            
            pyplot.xlim([-xy_buffer, (img_size[0] + xy_buffer)])
            pyplot.ylim([-xy_buffer, (img_size[1] + xy_buffer)])
            ax.set_aspect('equal', 'box')
            pyplot.xlabel("x (\")", fontsize=20)
            pyplot.ylabel("y (\")", fontsize=20)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'xydxdy_2')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Quiver plot for bright stars
        if scene_type != 'grid':
            # quiv_legend_bright = round(quiv_legend * quiv_scale_bright /
            #                            quiv_scale, 2)
            quiv_legend_bright = quiv_legend_2
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            quiv = pyplot.quiver(tab_in_bright.columns['x'][idx_in_bright],
                                 tab_in_bright.columns['y'][idx_in_bright],
                                 dx_bright * img_ps, dy_bright * img_ps, scale=quiv_scale_bright,
                                 scale_units='height', width=0.01)
            pyplot.quiverkey(quiv, 0.4, 0.9, quiv_legend_bright / 1e3,
                             (str(quiv_legend_bright) + " mas"), labelpos='W',
                             coordinates='figure', fontproperties={'size': 20})

            # Temp stuff, to use arcsec instead of px as axis units
            ticks_1 = numpy.arange(-8, 8, 2)
            ticks_str_1 = [str(i) for i in numpy.around(ticks_1, decimals=10)]
            ticks_orig_1 = (ticks_1 / img_ps) + (img_shape[0] / 2)
            ax.set_xticks(ticks_orig_1)
            ax.set_yticks(ticks_orig_1)
            ax.set_xticklabels(ticks_str_1)
            ax.set_yticklabels(ticks_str_1)
            
            pyplot.xlim([-xy_buffer, (img_size[0] + xy_buffer)])
            pyplot.ylim([-xy_buffer, (img_size[1] + xy_buffer)])
            ax.set_aspect('equal', 'box')
            pyplot.xlabel("x (\")", fontsize=20)
            pyplot.ylabel("y (\")", fontsize=20)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'xydxdy_bright')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Magnitude residual map for bright stars
        if scene_type != 'grid':
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            scatter = pyplot.scatter(tab_in_bright.columns['x'][idx_in_bright],
                                     tab_in_bright.columns['y'][idx_in_bright],
                                     s=20, c=dr_bright,
                                     cmap=pyplot.get_cmap('plasma_r'), vmin=0,
                                     vmax=dr_max_bright)
            pyplot.xlim([-xy_buffer, (img_size[0] + xy_buffer)])
            pyplot.ylim([-xy_buffer, (img_size[1] + xy_buffer)])
            ax.set_aspect('equal', 'box')
            pyplot.xlabel("x (px)", fontsize=20)
            pyplot.ylabel("y (px)", fontsize=20)
            pyplot.tick_params(labelsize=20)
            colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
            colorbar_ax_tmp = fig.colorbar(scatter, cax=colorbar_ax,
                                           orientation='horizontal')
            colorbar_ax.xaxis.set_ticks_position('top')
            colorbar_ax.tick_params(labelsize=20)
            colorbar_ax.set_xlabel(r"$\Delta$r (px)", fontsize=20)
            colorbar_ax_tmp.set_ticks(numpy.arange(0, (dr_max_bright +
                                      (dr_max_bright / 4)), (dr_max_bright /
                                                             4)))
            save_path = path.join(folder_out, 'xydr_bright')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Magnitude residual map for bright stars
        if scene_type != 'grid':
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            scatter = pyplot.scatter(tab_in_bright.columns['x'][idx_in_bright],
                                     tab_in_bright.columns['y'][idx_in_bright],
                                     s=20, c=dm_bright,
                                     cmap=pyplot.get_cmap('plasma_r'),
                                     vmin=-dm_range_bright,
                                     vmax=dm_range_bright)
            pyplot.xlim([-xy_buffer, (img_size[0] + xy_buffer)])
            pyplot.ylim([-xy_buffer, (img_size[1] + xy_buffer)])
            ax.set_aspect('equal', 'box')
            pyplot.xlabel("x (px)", fontsize=20)
            pyplot.ylabel("y (px)", fontsize=20)
            pyplot.tick_params(labelsize=20)
            colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
            fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
            colorbar_ax.xaxis.set_ticks_position('top')
            colorbar_ax.tick_params(labelsize=20)
            colorbar_ax.set_xlabel(r"$\Delta$m", fontsize=20)
            save_path = path.join(folder_out, 'xydm_bright')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Histogram of position residual
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.hist(dr, bins=numpy.arange(0, max(dr), dr_histbin), color='b')
        pyplot.xlim([0, dr_range])
        pyplot.ylim([0, dr_histmax])
        pyplot.xlabel(r"$\Delta$r (px)", fontsize=20)
        pyplot.ylabel("N", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'dr')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Second histogram of position residual
        if dr_histbin_2 and dr_range_2 and dr_histmax_2:
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.hist(dr, bins=numpy.arange(0, max(dr), dr_histbin_2),
                        color='b')
            pyplot.xlim([0, dr_range_2])
            pyplot.ylim([0, dr_histmax_2])
            pyplot.xlabel(r"$\Delta$r (px)", fontsize=20)
            pyplot.ylabel("N", fontsize=20)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'dr_2')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Histogram of position residual with text
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.hist(dr, bins=numpy.arange(0, max(dr), dr_histbin), color='b')
        pyplot.text((dr_range / 5), (9 * dr_histmax / 10),
                    (r"$\overline{\Delta r}$=" +
                     " {0:.5f} px".format(dr_mean_all)), ha='left', fontsize=20)
        pyplot.xlim([0, dr_range])
        pyplot.ylim([0, dr_histmax])
        pyplot.xlabel(r"$\Delta$r (px)", fontsize=20)
        pyplot.ylabel("N", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'dr_text')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Histogram of detections by magnitude
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.hist(m, bins=numpy.arange(m_range[0], m_range[1], m_histbin),
                    color='b')
        pyplot.xlim(m_range)
        pyplot.ylim([0, m_histmax])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel("N", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'm')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Histogram of detections by magnitude with text
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.hist(m, bins=numpy.arange(m_range[0], m_range[1], m_histbin),
                    color='b')
        pyplot.text((m_range[0] + 0.2), (3 * m_histmax / 4),
                    (r"$N_{match}$=" + " {0:d}".format(n_stars)), ha='left',
                    fontsize=20)
        pyplot.xlim(m_range)
        pyplot.ylim([0, m_histmax])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel("N", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'm_text')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Histogram of magnitude residual
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.hist(dm, bins=numpy.arange(-dm_tol, dm_tol, dm_histbin),
                    color='b')
        pyplot.xlim([-dm_range, dm_range])
        pyplot.ylim([0, dm_histmax])
        pyplot.xlabel(r"$\Delta$m", fontsize=20)
        pyplot.ylabel("N", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'dm')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Second histogram of magnitude residual
        if dm_histbin_2 and dm_range_2 and dm_histmax_2:
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.hist(dm, bins=numpy.arange(-dm_tol, dm_tol, dm_histbin_2),
                        color='b')
            pyplot.xlim([0, dm_range_2])
            pyplot.ylim([0, dm_histmax_2])
            pyplot.xlabel(r"$\Delta$m", fontsize=20)
            pyplot.ylabel("N", fontsize=20)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'dm_2')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Histogram of magnitude residual with text
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.hist(dm, bins=numpy.arange(-dm_tol, dm_tol, dm_histbin),
                    color='b')
        pyplot.text((dm_range / 10), (11 * dm_histmax / 12),
                    (r"$\overline{|\Delta m|}$=" +
                     " {0:.6f}".format(dm_mean_all)),
                    ha='left', fontsize=20)
        pyplot.xlim([-dm_range, dm_range])
        pyplot.ylim([0, dm_histmax])
        pyplot.xlabel(r"$\Delta$m", fontsize=20)
        pyplot.ylabel("N", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'dm_text')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Missed sources map
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])

        if len(tab_in_missed) > 0:
            scatter = pyplot.scatter(x_missed, y_missed, s=10, c=m_missed,
                                     cmap=pyplot.get_cmap('plasma'),
                                     vmin=m_range_color[0],
                                     vmax=m_range_color[1])

        pyplot.xlim([-xy_buffer, (img_size[0] + xy_buffer)])
        pyplot.ylim([-xy_buffer, (img_size[1] + xy_buffer)])
        ax.set_aspect('equal', 'box')
        pyplot.xlabel("x (px)", fontsize=20)
        pyplot.ylabel("y (px)", fontsize=20)
        pyplot.tick_params(labelsize=20)

        if len(tab_in_missed) > 0:
            colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
            fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')

        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("m", fontsize=20)
        save_path = path.join(folder_out, 'xym_missed')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Missed sources magnitude histogram
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.hist(m_missed, bins=numpy.arange(m_range[0], m_range[1],
                    missed_histbin), color='b')
        pyplot.xlim(m_range)
        pyplot.ylim([0, missed_histmax])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel("N", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'm_missed')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Missed sources magnitude histogram with text
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.hist(m_missed, bins=numpy.arange(m_range[0], m_range[1],
                                                missed_histbin), color='b')
        pyplot.text((m_range[0] + (m_range[1] - m_range[0]) / 10), (9 *
                    missed_histmax / 10), "Missed sources: " +
                    "{0}".format(len(idx_missed_in)), ha='left', fontsize=20)
        pyplot.xlim(m_range)
        pyplot.ylim([0, missed_histmax])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel("N", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'm_missed_text')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Fake sources map
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])

        if len(tab_out_fake) > 0:
            scatter = pyplot.scatter(x_fake, y_fake, s=10, c=m_fake,
                                     cmap=pyplot.get_cmap('plasma'),
                                     vmin=m_range_fake_color[0],
                                     vmax=m_range_fake_color[1])

        pyplot.xlim([-xy_buffer, (img_size[0] + xy_buffer)])
        pyplot.ylim([-xy_buffer, (img_size[1] + xy_buffer)])
        ax.set_aspect('equal', 'box')
        pyplot.xlabel("x (px)", fontsize=20)
        pyplot.ylabel("y (px)", fontsize=20)
        pyplot.tick_params(labelsize=20)

        if len(tab_out_fake) > 0:
            colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
            fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')

        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("m", fontsize=20)
        save_path = path.join(folder_out, 'xym_fake')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Fake sources magnitude histogram
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.hist(m_fake, bins=numpy.arange(m_range_fake[0], m_range_fake[1],
                                              fake_histbin), color='b')
        pyplot.xlim(m_range_fake)
        pyplot.ylim([0, fake_histmax])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel("N", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'm_fake')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Fake sources magnitude histogram with text
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.hist(m_fake, bins=numpy.arange(m_range_fake[0], m_range_fake[1],
                                              fake_histbin), color='b')
        pyplot.text((m_range_fake[0] + (m_range_fake[1] - m_range_fake[0]) /
                    10), (9 * fake_histmax / 10), "Fake sources: " +
                    "{0}".format(len(idx_fake)), ha='left', fontsize=20)
        pyplot.xlim(m_range_fake)
        pyplot.ylim([0, fake_histmax])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel("N", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'm_fake_text')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Position vs position residual
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.scatter(r, dr*img_ps*1e3, s=5, c='k')
        pyplot.xlim([0, (numpy.hypot(img_size[0], img_size[1]) / 2)])
        pyplot.ylim([0, dr_range*img_ps*1e3])
        ax.set_xticks(r_ticks_orig)
        ax.set_xticklabels([str(int(i)) for i in r_ticks])
        pyplot.xlabel("R (\")", fontsize=20)
        pyplot.ylabel(r"$\Delta$r (mas)", fontsize=20, labelpad=-2)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'rdr')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Second position vs position residual
        if dr_range_2:
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.scatter(r, dr, s=5, c='k')
            pyplot.xlim([0, (numpy.hypot(img_size[0], img_size[1]) / 2)])
            pyplot.ylim([0, dr_range_2])
            ax.set_xticks(r_ticks_orig)
            ax.set_xticklabels([str(int(i)) for i in r_ticks])
            pyplot.xlabel("R (\")", fontsize=20)
            pyplot.ylabel(r"$\Delta$r (px)", fontsize=20, labelpad=-2)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'rdr_2')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Position vs position residual, colored by magnitude
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(r, dr, s=5, c=m,
                                 cmap=pyplot.get_cmap('plasma'),
                                 vmin=m_range_color[0], vmax=m_range_color[1])
        pyplot.xlim([0, (numpy.hypot(img_size[0], img_size[1]) / 2)])
        pyplot.ylim([0, dr_range])
        ax.set_xticks(r_ticks_orig)
        ax.set_xticklabels([str(int(i)) for i in r_ticks])
        pyplot.xlabel("R (\")", fontsize=20)
        pyplot.ylabel(r"$\Delta$r (px)", fontsize=20, labelpad=-2)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("m", fontsize=20)
        save_path = path.join(folder_out, 'rdrm')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Position vs magnitude residual
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.scatter(r, dm, s=5, c='k')
        pyplot.xlim([0, (numpy.hypot(img_size[0], img_size[1]) / 2)])
        pyplot.ylim([-dm_range, dm_range])
        ax.set_xticks(r_ticks_orig)
        ax.set_xticklabels([str(int(i)) for i in r_ticks])
        pyplot.xlabel("R (\")", fontsize=20)
        pyplot.ylabel(r"$\Delta$m", fontsize=20, labelpad=-2)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'rdm')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Second position vs magnitude residual
        if dm_range_2:
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.scatter(r, dm, s=5, c='k')
            pyplot.xlim([0, (numpy.hypot(img_size[0], img_size[1]) / 2)])
            pyplot.ylim([-dm_range_2, dm_range_2])
            ax.set_xticks(r_ticks_orig)
            ax.set_xticklabels([str(int(i)) for i in r_ticks])
            pyplot.xlabel("R (\")", fontsize=20)
            pyplot.ylabel(r"$\Delta$m", fontsize=20, labelpad=-2)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'rdm_2')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Position vs magnitude residual, colored by magnitude
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(r, dm, s=5, c=m,
                                 cmap=pyplot.get_cmap('plasma'),
                                 vmin=m_range_color[0], vmax=m_range_color[1])
        pyplot.xlim([0, (numpy.hypot(img_size[0], img_size[1]) / 2)])
        pyplot.ylim([-dm_range, dm_range])
        ax.set_xticks(r_ticks_orig)
        ax.set_xticklabels([str(int(i)) for i in r_ticks])
        pyplot.xlabel("R (\")", fontsize=20)
        pyplot.ylabel(r"$\Delta$m", fontsize=20, labelpad=-2)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("m", fontsize=20)
        save_path = path.join(folder_out, 'rdmm')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude vs position residual
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.scatter(m, dr*img_ps*1e3, s=5, c='k')
        pyplot.xlim(m_range)
        pyplot.ylim([0, dr_range*img_ps*1e3])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel(r"$\Delta$r (mas)", fontsize=20, labelpad=-1)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'mdr')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude vs position residual with text
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.scatter(m, dr, s=5, c='k')

        if scene_type != 'grid':
            pyplot.text((m_range[0] + ((m_range[1] - m_range[0]) / 10)),
                        (5 * dr_range / 6), r"$\overline{\Delta r}_{m\leq" +
                        str(bright_mag) + "}$=" +
                        " {0:.4f} px".format(dr_mean_bright), ha='left',
                        fontsize=20)

        pyplot.xlim(m_range)
        pyplot.ylim([0, dr_range])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel(r"$\Delta$r (px)", fontsize=20, labelpad=-1)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'mdr_text')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude vs position residual, colored by position
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(m, dr, s=5, c=r,
                                 cmap=pyplot.get_cmap('plasma_r'), vmin=0,
                                 vmax=(numpy.hypot(img_size[0], img_size[1]) /
                                       2))

        if scene_type != 'grid':
            pyplot.text((m_range[0] + ((m_range[1] - m_range[0]) / 10)), (5 *
                        dr_range / 6), r"$\overline{\Delta r}_{m\leq" +
                        str(bright_mag) + "}$=" +
                        " {0:.4f} px".format(dr_mean_bright), ha='left',
                        fontsize=20)

        pyplot.xlim(m_range)
        pyplot.ylim([0, dr_range])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel(r"$\Delta$r (px)", fontsize=20, labelpad=-1)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("R (px)", fontsize=20)
        save_path = path.join(folder_out, 'mdrr')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude vs magnitude residual
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.scatter(m, dm, s=5, c='k')
        pyplot.xlim(m_range)
        pyplot.ylim([-dm_range, dm_range])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel(r"$\Delta$m", fontsize=20, labelpad=-1)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'mdm')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude vs magnitude residual with text
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.scatter(m, dm, s=5, c='k')

        if scene_type != 'grid':
            pyplot.text((m_range[0] + ((m_range[1] - m_range[0]) / 10)),
                        (3 * dm_range / 5), r"$\overline{|\Delta m|}_{m\leq" +
                        str(bright_mag) + "}$=" +
                        " {0:.4f}".format(dm_mean_bright), ha='left',
                        fontsize=20)

        pyplot.xlim(m_range)
        pyplot.ylim([-dm_range, dm_range])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel(r"$\Delta$m", fontsize=20, labelpad=-1)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'mdm_text')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude vs magnitude residual, colored by position
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(m, dm, s=5, c=r,
                                 cmap=pyplot.get_cmap('plasma_r'), vmin=0,
                                 vmax=(numpy.hypot(img_size[0], img_size[1]) /
                                       2))

        if scene_type != 'grid':
            pyplot.text((m_range[0] + ((m_range[1] - m_range[0]) / 10)), (3 *
                        dm_range / 5), r"$\overline{|\Delta m|}_{m\leq" +
                        str(bright_mag) + "}$=" +
                        " {0:.4f}".format(dm_mean_bright), ha='left',
                        fontsize=20)

        pyplot.xlim(m_range)
        pyplot.ylim([-dm_range, dm_range])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel(r"$\Delta$m", fontsize=20, labelpad=-1)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("R (px)", fontsize=20)
        save_path = path.join(folder_out, 'mdmr')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Position residual vs magnitude residual
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.scatter(dr, dm, s=5, c='k')
        pyplot.xlim([0, dr_range])
        pyplot.ylim([-dm_range, dm_range])
        pyplot.xlabel(r"$\Delta$r (px)", fontsize=20)
        pyplot.ylabel(r"$\Delta$m", fontsize=20, labelpad=-1)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'drdm')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Position residual vs magnitude residual, colored by magnitude
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(dr, dm, s=5, c=m,
                                 cmap=pyplot.get_cmap('plasma'),
                                 vmin=m_range_color[0], vmax=m_range_color[1])
        pyplot.xlim([0, dr_range])
        pyplot.ylim([-dm_range, dm_range])
        pyplot.xlabel(r"$\Delta$r (px)", fontsize=20)
        pyplot.ylabel(r"$\Delta$m", fontsize=20, labelpad=-1)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("m", fontsize=20)
        save_path = path.join(folder_out, 'drdmm')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Position residual vs FVU, colored by magnitude
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(dr, fvu, s=5, c=m,
                                 cmap=pyplot.get_cmap('plasma'),
                                 vmin=m_range_color[0], vmax=m_range_color[1],
                                 label=None)
        ax.set_xscale('log')
        ax.set_yscale('log')
        pyplot.xlim([dr_fvu_min, dr_range])
        pyplot.ylim([(10 ** i) for i in fvu_range])
        pyplot.xlabel(r"$\Delta$r (px)", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("m", fontsize=20)
        save_path = path.join(folder_out, 'drfvum')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Position residual vs FVU, colored by magnitude with fit
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(dr, fvu, s=5, c=m,
                                 cmap=pyplot.get_cmap('plasma'),
                                 vmin=m_range_color[0], vmax=m_range_color[1],
                                 label=None)
        pyplot.plot([dr_fvu_min, dr_range], (10 ** (numpy.log10([dr_fvu_min,
                    dr_range]) * fit_dr_fvu[0] + fit_dr_fvu[1])), 'r-',
                    label="Slope = {0:.2f}\nIntercept = {0:.2f}".format(
                    fit_dr_fvu[0], fit_dr_fvu[1]))
        ax.set_xscale('log')
        ax.set_yscale('log')
        pyplot.xlim([dr_fvu_min, dr_range])
        pyplot.ylim([(10 ** i) for i in fvu_range])
        ax.legend(loc='upper left', fontsize=17)
        pyplot.xlabel(r"$\Delta$r (px)", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("m", fontsize=20)
        save_path = path.join(folder_out, 'drfvum_fit')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude residual vs FVU, colored by magnitude
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(numpy.abs(dm), fvu, s=5, c=m,
                                 cmap=pyplot.get_cmap('plasma'),
                                 vmin=m_range_color[0], vmax=m_range_color[1],
                                 label=None)
        ax.set_xscale('log')
        ax.set_yscale('log')
        pyplot.xlim([dm_fvu_min, dm_range])
        pyplot.ylim([(10 ** i) for i in fvu_range])
        pyplot.xlabel(r"$\Delta$m", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("m", fontsize=20)
        save_path = path.join(folder_out, 'dmfvum')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Position vs FVU
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.scatter(r, fvu, s=5, c='k', label=None)
        ax.set_yscale('log')
        pyplot.xlim([0, (numpy.hypot(img_size[0], img_size[1]) / 2)])
        pyplot.ylim([(10 ** i) for i in fvu_range])
        ax.set_xticks(r_ticks_orig)
        ax.set_xticklabels([str(int(i)) for i in r_ticks])
        pyplot.xlabel("R (\")", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'rfvu')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Position vs FVU, colored by magnitude
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(r, fvu, s=5, c=m,
                                 cmap=pyplot.get_cmap('plasma'),
                                 vmin=m_range_color[0], vmax=m_range_color[1],
                                 label=None)
        ax.set_yscale('log')
        pyplot.xlim([0, (numpy.hypot(img_size[0], img_size[1]) / 2)])
        pyplot.ylim([(10 ** i) for i in fvu_range])
        ax.set_xticks(r_ticks_orig)
        ax.set_xticklabels([str(int(i)) for i in r_ticks])
        pyplot.xlabel("R (\")", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("m", fontsize=20)
        save_path = path.join(folder_out, 'rfvum')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # FVU map for bright stars
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(x, y, s=10, c=fvu,
                                 cmap=pyplot.get_cmap('plasma_r'),
                                 norm=colors.LogNorm(
                                     vmin=(10 ** fvu_range_color[0]),
                                     vmax=(10 ** fvu_range_color[1])))
        pyplot.xlim([-xy_buffer, (img_size[0] + xy_buffer)])
        pyplot.ylim([-xy_buffer, (img_size[1] + xy_buffer)])
        ax.set_aspect('equal', 'box')
        pyplot.xlabel("x (px)", fontsize=20)
        pyplot.ylabel("y (px)", fontsize=20)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel(r"$\Delta$r (px)", fontsize=20)
        save_path = path.join(folder_out, 'xyfvu')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude vs FVU
        print()
        aaa = numpy.where(m<=14)
        bbb = fvu[aaa]
        ccc = numpy.median(bbb)
        print(ccc)
        print()
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.scatter(m, fvu, s=5, c='k', label=None)
        ax.set_yscale('log')
        pyplot.xlim(m_range)
        pyplot.ylim([(10 ** i) for i in fvu_range])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'mfvu')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude vs FVU, colored by position
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(m, fvu, s=5, c=r,
                                 cmap=pyplot.get_cmap('plasma'), vmin=0,
                                 vmax=(numpy.hypot(img_size[0], img_size[1]) /
                                       2), label=None)
        ax.set_yscale('log')
        pyplot.xlim(m_range)
        pyplot.ylim([(10 ** i) for i in fvu_range])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("R (px)", fontsize=20)
        save_path = path.join(folder_out, 'mfvur')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Position residual vs FVU, colored by position
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(dr, fvu, s=5, c=r,
                                 cmap=pyplot.get_cmap('plasma_r'), vmin=0,
                                 vmax=(numpy.hypot(img_size[0], img_size[1]) /
                                       2), label=None)
        ax.set_xscale('log')
        ax.set_yscale('log')
        pyplot.xlim([dr_fvu_min, dr_range])
        pyplot.ylim([(10 ** i) for i in fvu_range])
        pyplot.xlabel(r"$\Delta$r (px)", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("R (px)", fontsize=20)
        save_path = path.join(folder_out, 'drfvur')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Position residual vs FVU, colored by position with fit
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(dr, fvu, s=5, c=r,
                                 cmap=pyplot.get_cmap('plasma_r'), vmin=0,
                                 vmax=(numpy.hypot(img_size[0], img_size[1]) /
                                       2), label=None)
        pyplot.plot([dr_fvu_min, dr_range], (10 ** (numpy.log10([dr_fvu_min,
                    dr_range]) * fit_dr_fvu[0] + fit_dr_fvu[1])), 'r-',
                    label="Slope = {0:.2f}\nIntercept = {0:.2f}".format(
                    fit_dr_fvu[0], fit_dr_fvu[1]))
        ax.set_xscale('log')
        ax.set_yscale('log')
        pyplot.xlim([dr_fvu_min, dr_range])
        pyplot.ylim([(10 ** i) for i in fvu_range])
        ax.legend(loc='upper left', fontsize=17)
        pyplot.xlabel(r"$\Delta$r (px)", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("R (px)", fontsize=20)
        save_path = path.join(folder_out, 'drfvur_fit')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude residual vs FVU, colored by position
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(numpy.abs(dm), fvu, s=5, c=m,
                                 cmap=pyplot.get_cmap('plasma'),
                                 vmin=m_range_color[0], vmax=m_range_color[1],
                                 label=None)
        ax.set_xscale('log')
        ax.set_yscale('log')
        pyplot.xlim([dm_fvu_min, dm_range])
        pyplot.ylim([(10 ** i) for i in fvu_range])
        pyplot.xlabel(r"$\Delta$m", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("R (px)", fontsize=20)
        save_path = path.join(folder_out, 'dmfvur')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude vs FVU, with fake sources
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.scatter(m, fvu, s=5, c='k', label="Matched")
        pyplot.scatter(m_fake, fvu_fake, s=5, c='r', label="Fake")
        ax.set_yscale('log')
        pyplot.xlim(m_range_fake)
        pyplot.ylim([(10 ** i) for i in fvu_range_fake])
        ax.legend(loc='upper left', fontsize=20)
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'mfvu_fake')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Pixel phase
        if px_phase:
            fit_points = numpy.arange(0, 1, 0.01)
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.scatter(x_phase, dx, s=10, c='b', label='x')
            pyplot.scatter(y_phase, dy, s=10, c='r', label='y')
            pyplot.plot(fit_points, fit_sine(fit_points, x_phase_fit[0],
                        x_phase_fit[1], x_phase_fit[2], x_phase_fit[3]), '-b')
            pyplot.plot(fit_points, fit_sine(fit_points, y_phase_fit[0],
                        y_phase_fit[1], y_phase_fit[2], y_phase_fit[3]), '-r')
            pyplot.xlim([0, 1])
            pyplot.ylim([-dr_range, dr_range])
            ax.legend(loc='upper right', fontsize=20)
            pyplot.xlabel("Phase (px)", fontsize=20)
            pyplot.ylabel("Position residual (px)", fontsize=20, labelpad=-3)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'px_phase')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Pixel phase with text
        if px_phase:
            fit_points = numpy.arange(0, 1, 0.01)
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.scatter(x_phase, dx, s=10, c='b', label='x')
            pyplot.scatter(y_phase, dy, s=10, c='r', label='y')
            pyplot.plot(fit_points, fit_sine(fit_points, x_phase_fit[0],
                        x_phase_fit[1], x_phase_fit[2], x_phase_fit[3]), '-b')
            pyplot.plot(fit_points, fit_sine(fit_points, y_phase_fit[0],
                        y_phase_fit[1], y_phase_fit[2], y_phase_fit[3]), '-r')
            pyplot.text(0.1, -(6 * dr_range / 10), (r"x$_{res,nofit,mean}$=" +
                        " {0:.4f} px".format(x_phase_res_nofit)), ha='left',
                        fontsize=20)
            pyplot.text(0.1, -(7 * dr_range / 10), (r"y$_{res,nofit,mean}$=" +
                        " {0:.4f} px".format(y_phase_res_nofit)), ha='left',
                        fontsize=20)
            pyplot.text(0.1, -(8 * dr_range / 10), (r"x$_{res,fit,mean}$=" +
                        " {0:.5f} px".format(x_phase_res_fit)), ha='left',
                        fontsize=20)
            pyplot.text(0.1, -(9 * dr_range / 10), (r"y$_{res,fit,mean}$=" +
                        " {0:.5f} px".format(y_phase_res_fit)), ha='left',
                        fontsize=20)
            pyplot.xlim([0, 1])
            pyplot.ylim([-dr_range, dr_range])
            ax.legend(loc='upper right', fontsize=20)
            pyplot.xlabel("Phase (px)", fontsize=20)
            pyplot.ylabel("Position residual (px)", fontsize=20, labelpad=-3)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'px_phase_text')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Grid phase
        if i_mode == 2:

            # Position grid phase
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.scatter(near_phase*img_ps, dr*img_ps*1e3, s=10, c='b')
            pyplot.ylim([0, dr_range_grid_phase*img_ps*1e3])
            pyplot.xlabel("Grid distance (\")", fontsize=20)
            pyplot.ylabel(r"$\Delta$r (mas)", fontsize=20,
                          labelpad=-3)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'grid_phase_dr')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

            # Magnitude grid phase
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.scatter(near_phase*img_ps, numpy.abs(dm), s=10, c='b')
            pyplot.ylim([0, dm_range_grid_phase])
            pyplot.xlabel("Grid distance (\")", fontsize=20)
            pyplot.ylabel(r"|$\Delta$m|", fontsize=20,
                          labelpad=-3)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'grid_phase_dm')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

    # End program
    print()
    print("Analysis completed\n")


def fit_sine(x, ampl, freq, phase, base):

    # Sine fitting function
    func = ampl * numpy.sin(freq * (x + phase)) + base

    return func
