"""Prepare folders and files for the on-sky GC test.

Data parameters:
    dir_test (string) - Test folder
    year (int) - Year of the fiber grid
"""

import shutil
from os import environ, path, remove, symlink

from astropy import table
import numpy

from airopa_test.generate_img import create_image_file, create_psfstars_file
from maosi.nirc2 import ao_opt_sims


# Data parameters
dir_test = ('/g/lu/scratch/jlu/work/ao/airopa/AIROPA_TEST/airopa_benchmarks/' +
            'grid_fiber_17_17')
year = 2017
n_stars = 81

# Start the program
print("\nPreparation started")
dir_airopa = environ['AIROPA_TEST_DATA']

if path.isfile(path.join(dir_test, 'image', 'fiber.fits')):
    remove(path.join(dir_test, 'image', 'fiber.fits'))

symlink(path.join(dir_airopa, 'fiber_grids', str(year), 'fiber.fits'),
        path.join(dir_test, 'image', 'fiber.fits'))

if path.isfile(path.join(dir_test, 'image', 'psf_stars_fiber.lis')):
    remove(path.join(dir_test, 'image', 'psf_stars_fiber.lis'))

if path.isfile(path.join(dir_test, 'image', 'psf_stars_fiber_force.lis')):
    remove(path.join(dir_test, 'image', 'psf_stars_fiber_force.lis'))

symlink(path.join(dir_airopa, 'fiber_grids', str(year), 'psf_stars_fiber.lis'),
        path.join(dir_test, 'image', 'psf_stars_fiber.lis'))
symlink(path.join(dir_airopa, 'fiber_grids', str(year),
                  'psf_stars_fiber_force.lis'),
        path.join(dir_test, 'image', 'psf_stars_fiber_force.lis'))
namess = ['dummy_star'] * n_stars
namess[0] = 'irs16C'
cat_pre = path.join(dir_test, 'image', 'psf_stars_fiber.lis')
tab_pre = table.Table.read(cat_pre, format='ascii', delimiter='\s')
x_poss = tab_pre['col4'].data
y_poss = tab_pre['col5'].data
xpos = -(((x_poss * 10) / 1024) - 5).flatten()
ypos = (((y_poss * 10) / 1024) - 5).flatten()
psf_tab = table.Table([namess, ([12] * n_stars), xpos, ypos, ([0.0] * n_stars),
                       ([0.0] * n_stars), ([2009.0] * n_stars),
                       (['-'] * n_stars), ([1] * n_stars)])
create_psfstars_file(dir_test, environ['AIROPA_DATA_PATH'], 'fiber', psf_tab)
psf_reference_str = (str(x_poss[0]) + '   ' + str(y_poss[0]) + '   # irs16C\n')
coo_file = open(path.join(dir_test, 'image', 'fiber.coo'), 'w')
coo_file.write(psf_reference_str)
coo_file.close()

# End program
print("Preparation completed\n")
