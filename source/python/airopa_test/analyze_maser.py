from datetime import datetime
from glob import glob
from os import makedirs, path
import warnings

from astropy import table
from astropy.io import fits
from astropy.utils.exceptions import AstropyUserWarning
from matplotlib import pyplot
import numpy

from flystar import align, starlists


def analyze_maser(dir_test, n_detect_min, m_range, dm_range_color, r_std_range,
                  dr_range, dr_text, quiv_scale, quiv_legend, dm_histbin,
                  dm_range, dm_histmax):

    # Start program
    print("\nAnalysis started")

    # Fixed parameters
    modes_str = ['legacy', 'single', 'variable']  # Name of AIROPA modes
    modes_color = ['r', 'g', 'b']  # Colors of pointings
    points_str = ['ce', 'nw', 'ne', 'sw', 'se']  # Short name of pointings
    points_str_long = ['Central', 'Northwest', 'Northeast', 'Southwest',
                       'Southeast']  # Long name of pointings
    points_color = ['k', 'r', 'g', 'b']  # Colors of pointings
    xy_buffer = 100  # Buffer in x-y images (px)

    # Load input data
    imgs = glob(path.join(dir_test, 'image', '*.fits'))
    imgs = [path.basename(imgs[i])[0: -5] for i in range(len(imgs))]
    imgs_points = [[], [], [], [], []]

    for i_point in range(5):
        imgs_points[i_point] = [x for x in imgs if (points_str[i_point] + '_')
                                in x]

    img_orig = fits.open(path.join(dir_test, 'image', (imgs_points[0][0] +
                                                       '.fits')))
    img_size = [img_orig[0].header['NAXIS1'], img_orig[0].header['NAXIS2']]

    # Save the parameters
    folder_out = path.join(dir_test, 'plot')

    if not path.exists(folder_out):
        makedirs(folder_out)

    now = datetime.now()
    params = open(path.join(dir_test, 'plot', 'params_plt.txt'), 'w')
    params.write('Date analyzed:            ' + str(now.year) + '-' +
                 str(now.month) + '-' + str(now.day) + '\n\n')
    params.write('--- PARAMETERS ---' + '\n')
    params.write('Minimum detection number: ' + str(n_detect_min) + '\n')
    params.close()

    # Analyze AIROPA modes
    sts = [[None] * 5] * 3
    sts_detect = [[None] * 5] * 3
    trs = [[None] * 5] * 3
    x = [[], [], []]
    y = [[], [], []]
    m = [[], [], []]
    dx = [[], [], []]
    dy = [[], [], []]
    dr = [[], [], []]
    dm = [[], [], []]

    for i_mode in range(0, 3):

        # Load output data
        print()
        print("--- AIROPA \"{0}\" mode ---".format(modes_str[i_mode]))
        folder_in = path.join(dir_test, 'fit', modes_str[i_mode])
        folder_out = path.join(dir_test, 'plot', modes_str[i_mode])

        if not path.exists(folder_out):
            makedirs(folder_out)

        # Analyze pointings
        sls_match_all = []

        for i_point in range(5):

            # Analyze observations
            sls = []

            for i_image, name_image in enumerate(imgs_points[i_point]):

                # Load output data
                print("\r{0} pointing, loading image {1} / {2}...".format(
                    points_str_long[i_point], (i_image + 1),
                    len(imgs_points[i_point])), end="")
                cat_out1 = glob(path.join(folder_in, (name_image +
                                '_*_stf.lis')))
                sls.append(starlists.StarList.from_lis_file(cat_out1[0],
                           error=False))
                cat_out2 = glob(path.join(folder_in, (name_image +
                                '_*_metrics.txt')))
                metrics_out = table.Table.read(cat_out2[0], format='ascii',
                                               data_start=0)
                sls[-1].add_column(table.Column(metrics_out[
                    metrics_out.colnames[0]] ** 2), name='fvu')
                sls[-1].add_column(table.Column(numpy.hypot((sls[-1]['x'] -
                                   (img_size[0] / 2)), (sls[-1]['y'] -
                                                        (img_size[1] / 2)))),
                                   name='r')

            print()

            # Match stars
            print("{0} pointing, matching stars...".format(
                points_str_long[i_point]))
            warnings.filterwarnings('ignore', category=AstropyUserWarning)
            sts[i_mode][i_point], trs[i_mode][i_point] = \
                align.mosaic_lists(sls, iters=5, dr_tol=(5 * [1.5]),
                                   dm_tol=(5 * [1]), trans_args=[{'order': 0},
                                   {'order': 0}, {'order': 0}, {'order': 0},
                                   {'order': 0}], update_ref_per_iter=False,
                                   verbose=False, outlier_tol=(5 * [None]))
            warnings.filterwarnings('default', category=AstropyUserWarning)
            sts[i_mode][i_point].combine_lists('fvu')
            sts[i_mode][i_point].combine_lists('r')
            sts[i_mode][i_point]['r_std2'] = (sts[i_mode][i_point]['x_std'] +
                                              sts[i_mode][i_point]['y_std']) / 2
            detect_idx = numpy.where(sts[i_mode][i_point]['n_detect'] >
                                     n_detect_min)[0]
            sts_detect[i_mode][i_point] = sts[i_mode][i_point][detect_idx]

            sls_match_all.append(starlists.StarList.from_table(table.Table(
                [sts_detect[i_mode][i_point]['name'],
                 sts_detect[i_mode][i_point]['x_avg'],
                 sts_detect[i_mode][i_point]['y_avg'],
                 sts_detect[i_mode][i_point]['m_avg'],
                 sts_detect[i_mode][i_point]['fvu_avg'],
                 sts_detect[i_mode][i_point]['r_avg'],
                 sts_detect[i_mode][i_point]['x_std'],
                 sts_detect[i_mode][i_point]['y_std']],
                names=['name', 'x', 'y', 'm', 'fvu', 'r', 'xe', 'ye'])))

            # Magnitude vs position STD
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.18, 0.1, 0.75, 0.75])
            pyplot.scatter(sts_detect[i_mode][i_point]['m_avg'],
                           sts_detect[i_mode][i_point]['r_std'], s=10, c='k')
            ax.set_yscale('log')
            pyplot.xlim(m_range)
            pyplot.ylim([(10 ** i) for i in r_std_range])
            pyplot.xlabel("m", fontsize=18)
            pyplot.ylabel(r"r$_{\sigma}$ (px)", fontsize=18)
            pyplot.tick_params(labelsize=15)
            save_path = path.join(folder_out, ('mrstd_' + points_str[i_point]))
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        print("Matching pointings...")
        dr_mean = []
        dm_std = []
        x_ce = sls_match_all[0]['x']
        y_ce = sls_match_all[0]['y']
        m_ce = sls_match_all[0]['m']

        for i_point in range(1, 5):

            sls_match = [sls_match_all[i] for i in [0, i_point]]
            sts_match, _ = align.mosaic_lists(sls_match, iters=5,
                                              dr_tol=(5 * [2]),
                                              dm_tol=(5 * [1]),
                                              trans_args=[{'order': 0},
                                                          {'order': 0},
                                                          {'order': 0},
                                                          {'order': 0},
                                                          {'order': 0}],
                                              update_ref_per_iter=False,
                                              verbose=False,
                                              outlier_tol=(5 * [None]))
            idx_match = numpy.where(sts_match['n_detect'] == 2)[0]
            x[i_mode].append(numpy.array([sts_match[idx_match]['x'][i][0] for i
                             in range(len(idx_match))]))
            y[i_mode].append(numpy.array([sts_match[idx_match]['y'][i][0] for i
                             in range(len(idx_match))]))
            m[i_mode].append(numpy.array([sts_match[idx_match]['m'][i][0] for i
                             in range(len(idx_match))]))
            dx[i_mode].append(numpy.array([sts_match[idx_match]['x'][i][1] for i
                              in range(len(idx_match))]) -
                              numpy.array(x_ce[idx_match]))
            dy[i_mode].append(numpy.array([sts_match[idx_match]['y'][i][1] for i
                              in range(len(idx_match))]) -
                              numpy.array(y_ce[idx_match]))
            dx[i_mode][-1] -= numpy.mean(dx[i_mode][-1])
            dy[i_mode][-1] -= numpy.mean(dy[i_mode][-1])
            dr[i_mode].append(numpy.hypot(dx[i_mode][i_point - 1],
                              dy[i_mode][i_point - 1]))
            dr_mean.append(numpy.mean(dr[i_mode][i_point - 1]))
            dm[i_mode].append(numpy.array([sts_match[idx_match]['m'][i][1] for i
                              in range(len(idx_match))]) -
                              numpy.array(m_ce[idx_match]))
            dm[i_mode][-1] -= numpy.mean(dm[i_mode][-1])
            dm_std.append(numpy.std(dm[i_mode][i_point - 1]))

        # Quiver plot of position difference
        print("Producing plots...")
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.18, 0.1, 0.75, 0.75])
        quiv = pyplot.quiver(x[i_mode][0], y[i_mode][0], dx[i_mode][0],
                             dy[i_mode][0], scale=quiv_scale,
                             scale_units='height', width=0.003,
                             color=points_color[0])
        pyplot.quiver(x[i_mode][1], y[i_mode][1], dx[i_mode][1], dy[i_mode][1],
                      scale=quiv_scale, scale_units='height', width=0.003,
                      color=points_color[1])
        pyplot.quiver(x[i_mode][2], y[i_mode][2], dx[i_mode][2], dy[i_mode][2],
                      scale=quiv_scale, scale_units='height', width=0.003,
                      color=points_color[2])
        pyplot.quiver(x[i_mode][3], y[i_mode][3], dx[i_mode][3], dy[i_mode][3],
                      scale=quiv_scale, scale_units='height', width=0.003,
                      color=points_color[3])
        pyplot.quiverkey(quiv, 0.4, 0.9, quiv_legend, (str(quiv_legend) +
                         " px"), labelpos='W', coordinates='figure',
                         fontproperties={'size': 18})
        pyplot.xlim([-xy_buffer, (img_size[0] + xy_buffer)])
        pyplot.ylim([-xy_buffer, (img_size[1] + xy_buffer)])
        ax.set_aspect('equal', 'box')
        pyplot.xlabel("x (px)", fontsize=18)
        pyplot.ylabel("y (px)", fontsize=18)
        pyplot.tick_params(labelsize=15)
        save_path = path.join(folder_out, 'xydxdy')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude vs position difference
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.18, 0.1, 0.75, 0.75])
        pyplot.scatter(m[i_mode][0], dr[i_mode][0], s=10, c=points_color[0])
        pyplot.scatter(m[i_mode][1], dr[i_mode][1], s=10, c=points_color[1])
        pyplot.scatter(m[i_mode][2], dr[i_mode][2], s=10, c=points_color[2])
        pyplot.scatter(m[i_mode][3], dr[i_mode][3], s=10, c=points_color[3])
        ax.set_yscale('log')
        pyplot.xlim(m_range)
        pyplot.ylim([(10 ** i) for i in dr_range])
        pyplot.xlabel("m", fontsize=18)
        pyplot.ylabel(r"$\Delta$r (px)", fontsize=18)
        pyplot.tick_params(labelsize=15)
        save_path = path.join(folder_out, 'mdr')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude vs position difference with text
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.18, 0.1, 0.75, 0.75])
        pyplot.scatter(m[i_mode][0], dr[i_mode][0], s=10, c=points_color[0])
        pyplot.scatter(m[i_mode][1], dr[i_mode][1], s=10, c=points_color[1])
        pyplot.scatter(m[i_mode][2], dr[i_mode][2], s=10, c=points_color[2])
        pyplot.scatter(m[i_mode][3], dr[i_mode][3], s=10, c=points_color[3])
        pyplot.text((((m_range[1] - m_range[0]) / 15) + m_range[0]), (10 **
                    dr_text), (r"$\overline{\Delta r}$=" +
                    " {0:.4f},".format(dr_mean[0])), ha='left', fontsize=13,
                    color=points_color[0])
        pyplot.text((((m_range[1] - m_range[0]) / 15) + m_range[0] + 2.2), (10
                    ** dr_text), ("{0:.4f},".format(dr_mean[1])), ha='left',
                    fontsize=13, color=points_color[1])
        pyplot.text((((m_range[1] - m_range[0]) / 15) + m_range[0] + 3.6), (10
                    ** dr_text), ("{0:.4f},".format(dr_mean[2])), ha='left',
                    fontsize=13, color=points_color[2])
        pyplot.text((((m_range[1] - m_range[0]) / 15) + m_range[0] + 5), (10 **
                    dr_text), ("{0:.4f} px".format(dr_mean[3])), ha='left',
                    fontsize=13, color=points_color[3])
        ax.set_yscale('log')
        pyplot.xlim(m_range)
        pyplot.ylim([(10 ** i) for i in dr_range])
        pyplot.xlabel("m", fontsize=18)
        pyplot.ylabel(r"$\Delta$r (px)", fontsize=18)
        pyplot.tick_params(labelsize=15)
        save_path = path.join(folder_out, 'mdr_text')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude difference map
        for i_point in range(4):
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.18, 0.1, 0.75, 0.75])
            scatter = pyplot.scatter(x[i_mode][i_point], y[i_mode][i_point],
                                     s=10, c=dm[i_mode][i_point],
                                     cmap=pyplot.get_cmap('plasma'),
                                     vmin=dm_range_color[0],
                                     vmax=dm_range_color[1])
            pyplot.text(0, (-xy_buffer * 2 / 3), (r"$\sigma_{m}$=" +
                        " {0:.4f}".format(dm_std[i_point])), ha='left',
                        fontsize=13, color='k')
            pyplot.xlim([-xy_buffer, (img_size[0] + xy_buffer)])
            pyplot.ylim([-xy_buffer, (img_size[1] + xy_buffer)])
            ax.set_aspect('equal', 'box')
            pyplot.xlabel("x (px)", fontsize=18)
            pyplot.ylabel("y (px)", fontsize=18)
            pyplot.tick_params(labelsize=15)
            colorbar_ax = fig.add_axes([0.18, 0.9, 0.75, 0.03])
            fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
            colorbar_ax.xaxis.set_ticks_position('top')
            colorbar_ax.tick_params(labelsize=15)
            colorbar_ax.set_xlabel(r"$\Delta$ m", fontsize=18)
            save_path = path.join(folder_out, ('xydm_' + points_str[i_point +
                                  1]))
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Magnitude difference histogram
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.18, 0.1, 0.75, 0.75])
        ax.hist(dm[i_mode], numpy.arange(-dm_range, dm_range, dm_histbin),
                histtype='step', fill=False, color=points_color,
                label=points_str_long[1: 5], linewidth=2)
        ax.legend(prop={'size': 10})
        pyplot.xlim([-dm_range, dm_range])
        pyplot.ylim([0, dm_histmax])
        pyplot.xlabel(r"$\Delta m$", fontsize=18)
        pyplot.ylabel("N", fontsize=18)
        pyplot.tick_params(labelsize=15)
        save_path = path.join(folder_out, 'dm')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

    for i_point in range(4):

        # Magnitude difference histogram
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.18, 0.1, 0.75, 0.75])
        ax.hist([dm[i][i_point] for i in range(3)], numpy.arange(-dm_range,
                dm_range, dm_histbin), histtype='step', fill=False,
                color=modes_color, label=modes_str, linewidth=3)
        ax.legend(prop={'size': 10})
        pyplot.xlim([-dm_range, dm_range])
        pyplot.ylim([0, dm_histmax])
        pyplot.xlabel(r"$\Delta m$", fontsize=18)
        pyplot.ylabel("N", fontsize=18)
        pyplot.tick_params(labelsize=15)
        save_path = path.join(dir_test, 'plot', ('dm_' + points_str[i_point +
                              1]))
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude difference histogram
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.18, 0.1, 0.75, 0.75])
        for i_mode in range(3):
            ax.scatter(m[i_mode][i_point], dm[i_mode][i_point], s=5,
                       color=modes_color[i_mode], label=modes_str[i_mode])
        ax.legend(prop={'size': 10})
        pyplot.xlim(m_range)
        pyplot.ylim([-dm_range, dm_range])
        pyplot.xlabel("m", fontsize=18)
        pyplot.ylabel(r"$\Delta m$", fontsize=18)
        pyplot.tick_params(labelsize=15)
        save_path = path.join(dir_test, 'plot', ('mdm_' + points_str[i_point +
                                                                     1]))
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

    # End program
    print()
    print("Analysis completed\n")
