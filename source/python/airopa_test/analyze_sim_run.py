"""Run AIROPA in different modes on a simulated image and produce plots to
compare the astrometry and photometry accuracy.

Variables with a name ending in '_2' have the same function of the corresponding
variables without the suffix, and are used to generate a second plot with
different parameters. If a second plot is not needed, use the value 'None' for
all of them.
Position and magnitude residuals are averaged to zero.

Data parameters:
    dir_test (string) - Test data folder
    scene_type (string) - Type of scene, either simulated GC ('gc') or grid of
        stars ('grid')
    psf_load (bool) - PSF was loaded from file

Analysis parameters:
    dr_tol (float) - Matching distance tolerance (px)
    dm_tol (float) - Matching magnitude tolerance
    bright_mag (float) - Magnitude cut to define "bright" stars. Should be where
        residuals in the 'mdr.png' and 'mdm.png' output plots start to increase
        due to noise

Plot parameters:
    img_orig_range (float [2]) - Range of values to show on the original image
    img_res_range (float [2]) - Range of values to show on the residual image
    test_pos (int [4, 2]) - Approximate x and y positions of eight bright test
        stars (2 top-left, 2 top-right, 2 bottom-left, 2 bottom-right of the
        image)
    test_r (float) - Radius of the test stars' imaes (px)
    quiv_scale (float) - Quiver plot scale
    quiv_scale_bright (float) - Quiver plot scale for bright stars
    quiv_legend (float) - Quantity used for the quiver plot legend
    dr_range (float) - Maximum positional residual shown (px)
    dr_range_grid_phase (float) - Maximum positional residual shown for the grid
        phase (px)
    dr_zoom (float) - Maximum position residual shown zooming in
    dr_histmax (int) - Maximum height in the position residual histogram
    dr_histbin (float) - Bin size in the position residual histogram
    m_range (float [2]) - Range of magnitudes plotted
    m_range_color (float [2]) - Range of magnitudes colored
    m_histmax (int) - Maximum height in the magnitude histogram
    m_histbin (float) - Bin size in the magnitude histogram
    dr_max_bright (float) - Maximum position residual shown for bright stars
    dm_histmax (int) - Maximum height in the magnitude residual histogram
    dm_histbin (float) - Bin size in the magnitude residual histogram
    dm_range (float) - Maximum magnitude residual shown
    dm_range_bright (float) - Maximum magnitude residual shown for bright stars
    dm_range_grid_phase (float) - Maximum magnitude residual shown for the grid
        phase
    missed_histmax (int) - Maximum height in the magnitude histogram of missed
        sources
    missed_histbin (float) - Bin size in the magnitude histogram of missed
        sources
    fake_histmax (int) - Maximum height in the magnitude histogram of fake
        sources
    fake_histbin (float) - Bin size in the magnitude histogram of fake sources
    m_range_fake (float [2]) - Minimum and maximum magnitude of fake sources
        shown
    fvu_range (float [2]) - Minimum and maximum FVU shown
    fvu_range_fake (float [2]) - Minimum and maximum FVU of fake sources shown
    dr_fvu_min (float) - Minimum position residual shown in the FVU plot
    dm_fvu_min (float) - Minimum magnitude residual shown in the FVU plot
    m_fvu (float) - Maximum magnitude used to fit the FVU
    px_phase (bool) - Analyze the pixel phase
"""

from airopa_test.analyze_sim import analyze_sim


# Data parameters
dir_test = ('/g/lu/scratch/jlu/work/ao/airopa/AIROPA_TEST/airopa_benchmarks/' +
            'grid_const')
scene_type = 'grid'
psf_load = False

# Analysis parameters
dr_tol = 4
dm_tol = 10
bright_mag = 14

# Plot parameters
img_orig_range = [5000, 500000]
img_res_range = [-10000, 30000]
img_res_range_2 = [-50000, 50000]
test_pos = [[134, 678], [399, 791], [746, 895], [909, 1006], [50, 166],
            [102, 41], [954, 348], [789, 225]]
test_r = 50
quiv_scale = 10
quiv_scale_2 = None
quiv_scale_bright = 1
quiv_legend = 1
quiv_legend_2 = None
dr_range = 1.5
dr_range_2 = None
dr_range_grid_phase = 0.07
dr_zoom = 0.4
dr_histmax = 800
dr_histmax_2 = None
dr_histbin = 0.05
dr_histbin_2 = None
m_range = [8, 21]
m_range_color = [12, 20]
m_histmax = 200
m_histbin = 0.5
dr_max_bright = 0.2
dm_histmax = 600
dm_histmax_2 = None
dm_histbin = 0.02
dm_histbin_2 = None
dm_range = 0.5
dm_range_2 = None
dm_range_bright = 0.12
dm_range_grid_phase = 0.025
missed_histmax = 200
missed_histbin = 0.5
fake_histmax = 40
fake_histbin = 0.5
m_range_fake = [8, 20]
m_range_fake_color = [14, 19]
fvu_range = [-6, 2]
fvu_range_color = [-4, 0]
fvu_range_fake = [-6, 2]
dr_fvu_min = 0.01
dm_fvu_min = 0.001
m_fvu = 19
px_phase = False

# Start program
analyze_sim(dir_test, scene_type, dr_tol, dm_tol, bright_mag, img_orig_range,
            img_res_range, img_res_range_2, test_pos, test_r, quiv_scale,
            quiv_scale_2, quiv_scale_bright, quiv_legend, quiv_legend_2,
            dr_range, dr_range_2, dr_range_grid_phase, dr_zoom, dr_histmax,
            dr_histmax_2, dr_histbin, dr_histbin_2, m_range, m_range_color,
            dr_max_bright, dm_histmax, dm_histmax_2, dm_histbin, dm_histbin_2,
            dm_range, dm_range_2, dm_range_bright, dm_range_grid_phase,
            missed_histmax, missed_histbin, fake_histmax, fake_histbin,
            m_range_fake, m_range_fake_color, fvu_range, fvu_range_color,
            fvu_range_fake, dr_fvu_min, dm_fvu_min, m_fvu, px_phase, psf_load,
            m_histmax, m_histbin)
