"""Calculate the coordinates in pixels of the T/T star in the mosaic images.
Mosaic images are ordered center, northwest, northeast, southwest and southeast.

Data parameters:
    tt_cal (float [2]) - Coordinate of the T/T star in the calibration image
        [px]
    coo_cal (float [2]) - Coordinate of the COO star in the calibration image
        [px]
    sgra_cal (float [2]) - Coordinate of the COO star in the calibration image
        ["]
    coo (float [5, 2]) - Coordinates of the COO stars in the mosaic images [px]
    sgra (float [5, 2]) - Coordinates of the COO stars in the mosaic images ["]
"""

import numpy as np


# Data parameters
tt_cal = [-392, 2463]
coo_cal = [425.26, 695.81]
sgra_cal = [1.083, 0.507]
coo = [[864.94, 443.62], [653.59, 203.79], [108.49, 452.39], [570.11, 748.13],
       [512.41, 642.31]]
sgra = [[1.083, 0.507], [0.053, 1.210], [11.554, 3.769], [1.083, 0.507],
        [7.516, -0.434]]

# Fixed parameters
px_s = 0.009952  # NIRC2 pixel scale [px/"]

# Coordinate of SgrA* in the calibration image [px]
sgra_cal_px = np.array([coo_cal] * 5) + (np.array([sgra_cal] * 5) / px_s)

# Coordinate of SgrA* in the mosaic images [px]
sgra_px = coo + (np.array(sgra) / px_s)

# Distance of the mosaic images' (0, 0) to the calibration image's (0, 0) [px]
zero_dist = sgra_cal_px - sgra_px

# Coordinate of the T/T star in the mosaic images [px]
tt = ([tt_cal] * 5) - zero_dist

# x oordinate of the T/T star in the mosaic images [px]
ttx = [round(i, 2) for i in tt[:, 0]]

# y oordinate of the T/T star in the mosaic images [px]
tty = [round(i, 2) for i in tt[:, 1]]

# Show results
print()
print('-----------------------------------------------------------------------')
print(' T/T star x coordinates: {0}'.format(ttx))
print(' T/T star y coordinates: {0}'.format(tty))
print('-----------------------------------------------------------------------')
print()
