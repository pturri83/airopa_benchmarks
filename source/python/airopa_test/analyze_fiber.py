from datetime import datetime
from glob import glob
from os import makedirs, path
import shutil
import time

from astropy import table
from astropy.io import fits
from matplotlib import pyplot, colors
import numpy


def analyze_fiber(dir_test, n_fibers, img_orig_range, img_res_range, test_pos,
                  test_r, m_range, m_range_2, m_range_color, fvu_range,
                  fvu_range_2, fvu_range_color, m_histmax, m_histbin):

    # Start program
    print("\nAnalysis started")

    # Save the parameters
    folder_out = path.join(dir_test, 'plot')

    if path.isdir(folder_out):

        for i in range(5):

            try:
                shutil.rmtree(folder_out)
                time.sleep(2)
            except:
                time.sleep(2)

    makedirs(folder_out)

    now = datetime.now()
    params = open(path.join(dir_test, 'plot', 'params_plt.txt'), 'w')
    params.write('Date analyzed:            ' + str(now.year) + '-' +
                 str(now.month) + '-' + str(now.day) + '\n')

    # Fixed parameters
    modes_str = ['legacy', 'single', 'variable']  # Name of AIROPA modes
    xy_buffer = 100  # Buffer in x-y images (px)
    img_orig = fits.open(path.join(dir_test, 'image', 'fiber.fits'))
    img_size = [img_orig[0].header['NAXIS1'], img_orig[0].header['NAXIS2']]
    img_ps = 0.009942  # Pixel scale

    # Find test stars
    cat_test = glob(path.join(path.join(dir_test, 'fit', 'variable'),
                              'fiber_*_stf.lis'))
    tab_test = table.Table.read(cat_test[0], format='ascii', delimiter='\s')
    tab_test.rename_column('col4', 'x')
    tab_test.rename_column('col5', 'y')
    tab_test.rename_column('col2', 'm')
    tests_idx = [-1] * 8

    for i_test in range(8):
        test_dist = numpy.hypot((tab_test['x'] - test_pos[i_test][0]),
                                (tab_test['y'] - test_pos[i_test][1]))
        tests_idx[i_test] = int(numpy.argmin(test_dist))
        test_pos[i_test] = [int(tab_test['x'][tests_idx[i_test]]),
                            int(tab_test['y'][tests_idx[i_test]])]

    # Show original image
    img_ticks = 2
    img_shape = img_orig[0].data.shape
    xy_px = numpy.array(img_shape)
    xy_ticks_max = numpy.floor((xy_px * img_ps) / (2 * img_ticks)) * img_ticks
    x_ticks = numpy.arange(-xy_ticks_max[1], (xy_ticks_max[1] + img_ticks), img_ticks)
    y_ticks = numpy.arange(-xy_ticks_max[0], (xy_ticks_max[0] + img_ticks), img_ticks)
    x_ticks_orig = (x_ticks / img_ps) + ((xy_px[1] - 1) / 2)
    y_ticks_orig = (y_ticks / img_ps) + ((xy_px[0] - 1) / 2)
    x_ticks_str = [str(i) for i in numpy.around(x_ticks, decimals=10)]
    y_ticks_str = [str(i) for i in numpy.around(y_ticks, decimals=10)]
    fig = pyplot.figure(figsize=(6, 6))
    ax = fig.add_subplot(111)
    pyplot.imshow(img_orig[0].data, clim=img_orig_range, cmap='hot',
                  aspect='equal')
    ax.set_position([0.25, 0.15, 0.65, 0.65])
    ax.invert_yaxis()
    ax.set_xticks(x_ticks_orig)
    ax.set_yticks(y_ticks_orig)
    ax.set_xticklabels(x_ticks_str)
    ax.set_yticklabels(y_ticks_str)
    pyplot.xlabel("x (\")", fontsize=20)
    pyplot.ylabel("y (\")", fontsize=20)
    pyplot.tick_params(labelsize=20)
    save_path = path.join(folder_out, 'image')
    pyplot.savefig(save_path + '.png')
    pyplot.savefig(save_path + '.pdf')
    pyplot.close(fig)

    # Show original test stars
    test_str = ['tl1', 'tl2', 'tr1', 'tr2', 'bl1', 'bl2', 'br1', 'br2']
    img_ticks_zoom = 0.1
    xy_ticks_max_zoom = numpy.floor((xy_px * img_ps) / (2 * img_ticks_zoom)) * img_ticks
    x_ticks_zoom = numpy.arange(-xy_ticks_max_zoom[1], (xy_ticks_max_zoom[1] + img_ticks_zoom), img_ticks_zoom)
    y_ticks_zoom = numpy.arange(-xy_ticks_max_zoom[0], (xy_ticks_max_zoom[0] + img_ticks_zoom), img_ticks_zoom)
    x_ticks_orig_zoom = (x_ticks_zoom / img_ps) + ((xy_px[1] - 1) / 2)
    y_ticks_orig_zoom = (y_ticks_zoom / img_ps) + ((xy_px[0] - 1) / 2)
    x_ticks_str_zoom = [str(i) for i in numpy.around(x_ticks_zoom, decimals=10)]
    y_ticks_str_zoom = [str(i) for i in numpy.around(y_ticks_zoom, decimals=10)]

    for i_test in range(8):
        fig = pyplot.figure(figsize=(6, 6))
        ax = fig.add_subplot(111)
        pyplot.imshow(img_orig[0].data, clim=img_orig_range, cmap='hot',
                      aspect='equal')
        ax.set_xticks(x_ticks_orig_zoom)
        ax.set_yticks(y_ticks_orig_zoom)
        ax.set_xticklabels(x_ticks_str_zoom)
        ax.set_yticklabels(y_ticks_str_zoom)
        pyplot.axis([(test_pos[i_test][0] - test_r), (test_pos[i_test][0] +
                    test_r), (test_pos[i_test][1] - test_r),
                    (test_pos[i_test][1] + test_r)])
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.xlabel("x (\")", fontsize=20)
        pyplot.ylabel("y (\")", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, ('image_test_' + test_str[i_test]))
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

    # Analyze AIROPA modes
    r_max = (numpy.hypot(img_size[0], img_size[1]) / 2) * img_ps
    r_ticks = numpy.arange(0, r_max, numpy.around((r_max / 5), decimals=0))
    r_ticks_orig = r_ticks / img_ps

    for i_mode in range(0, 3):

        # Load output data
        print("\rAnalyzing AIROPA \"{0}\" mode...".format(modes_str[i_mode]),
              end="")
        folder_in = path.join(dir_test, 'fit', modes_str[i_mode])
        img_res = fits.open(path.join(folder_in, 'fiber_res.fits'))
        cat_out1 = glob(path.join(folder_in, 'fiber_*_stf.lis'))
        tab_out = table.Table.read(cat_out1[0], format='ascii', delimiter='\s')
        tab_out.rename_column('col4', 'x')
        tab_out.rename_column('col5', 'y')
        tab_out.rename_column('col2', 'm')
        cat_out2 = glob(path.join(folder_in, 'fiber_*_metrics.txt'))
        metrics_out = table.Table.read(cat_out2[0], format='ascii',
                                       data_start=0)
        tab_out.add_column(table.Column(metrics_out[metrics_out.colnames[0]] **
                           2), name='fvu')

        # Match stars
        x_out = tab_out.columns['x']
        y_out = tab_out.columns['y']
        m_out = tab_out.columns['m']
        n_stars = len(m_out)
        fvu_out = tab_out.columns['fvu']
        r_out = numpy.hypot((x_out - (img_size[0] / 2)),
                            (y_out - (img_size[1] / 2)))

        # Calculate median FVU
        idx_bright = m_out.argsort()[: n_fibers]
        fvu_median_bright = numpy.median(fvu_out[idx_bright])

        # Show residual image
        folder_out = path.join(dir_test, 'plot', modes_str[i_mode])

        if not path.exists(folder_out):
            makedirs(folder_out)

        fig = pyplot.figure(figsize=(6, 6))
        ax = fig.add_subplot(111)
        pyplot.imshow(img_res[0].data, clim=img_res_range, cmap='hot',
                      aspect='equal')
        ax.set_xticks(x_ticks_orig_zoom)
        ax.set_yticks(y_ticks_orig_zoom)
        ax.set_xticklabels(x_ticks_str_zoom)
        ax.set_yticklabels(y_ticks_str_zoom)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        ax.invert_yaxis()
        pyplot.xlabel("x (\")", fontsize=20)
        pyplot.ylabel("y (\")", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'image_res')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Show residual test stars
        for i_test in range(8):
            fig = pyplot.figure(figsize=(6, 6))
            ax = fig.add_subplot(111)
            pyplot.imshow(img_res[0].data, clim=img_res_range, cmap='hot',
                          aspect='equal')
            ax.set_xticks(x_ticks_orig_zoom)
            ax.set_yticks(y_ticks_orig_zoom)
            ax.set_xticklabels(x_ticks_str_zoom)
            ax.set_yticklabels(y_ticks_str_zoom)
            pyplot.scatter(tab_out['x'], tab_out['y'], marker='o', c='g', s=70)
            pyplot.axis([(test_pos[i_test][0] - test_r), (test_pos[i_test][0] +
                        test_r), (test_pos[i_test][1] - test_r),
                        (test_pos[i_test][1] + test_r)])
            pyplot.text(0.3, 0.85,
                        "FVU = {0:.2e}".format(fvu_out[tests_idx[i_test]]),
                        ha='left', transform=fig.transFigure, fontsize=20)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.xlabel("x (\")", fontsize=20)
            pyplot.ylabel("y (\")", fontsize=20)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, ('image_res_test_' +
                                  test_str[i_test]))
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Found sources map
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(x_out, y_out, s=10, c=m_out,
                                 cmap=pyplot.get_cmap('plasma'),
                                 vmin=m_range_color[0], vmax=m_range_color[1])
        pyplot.xlim([-xy_buffer, (img_size[0] + xy_buffer)])
        pyplot.ylim([-xy_buffer, (img_size[1] + xy_buffer)])
        ax.set_aspect('equal', 'box')
        pyplot.xlabel("x (px)", fontsize=20)
        pyplot.ylabel("y (px)", fontsize=20)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("m", fontsize=20)
        save_path = path.join(folder_out, 'xym')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Histogram of detections by magnitude
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.hist(m_out, bins=numpy.arange(m_range[0], m_range[1], m_histbin),
                    color='b')
        pyplot.xlim(m_range)
        pyplot.ylim([0, m_histmax])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel("N", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'm')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Histogram of detections by magnitude with text
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.hist(m_out, bins=numpy.arange(m_range[0], m_range[1], m_histbin),
                    color='b')
        pyplot.text((m_range[0] + 2), (3 * m_histmax / 4),
                    (r"N=" + " {0:d}".format(n_stars)), ha='left', fontsize=20)
        pyplot.xlim(m_range)
        pyplot.ylim([0, m_histmax])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel("N", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'm_text')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Position vs FVU
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.scatter(r_out, fvu_out, s=5, c='k', label=None)
        ax.set_yscale('log')
        pyplot.xlim([0, (numpy.hypot(img_size[0], img_size[1]) / 2)])
        pyplot.ylim([(10 ** i) for i in fvu_range])
        ax.set_xticks(r_ticks_orig)
        ax.set_xticklabels([str(int(i)) for i in r_ticks])
        pyplot.xlabel("R (\")", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'rfvu')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Second position vs FVU
        if fvu_range_2:
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.scatter(r_out, fvu_out, s=5, c='k', label=None)
            ax.set_yscale('log')
            pyplot.xlim([0, (numpy.hypot(img_size[0], img_size[1]) / 2)])
            pyplot.ylim([(10 ** i) for i in fvu_range_2])
            ax.set_xticks(r_ticks_orig)
            ax.set_xticklabels([str(int(i)) for i in r_ticks])
            pyplot.xlabel("R (\")", fontsize=20)
            pyplot.ylabel("FVU", fontsize=20)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'rfvu_2')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Position vs FVU, colored by magnitude
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(r_out, fvu_out, s=5, c=m_out,
                                 cmap=pyplot.get_cmap('plasma'),
                                 vmin=m_range_color[0], vmax=m_range_color[1],
                                 label=None)
        ax.set_yscale('log')
        pyplot.xlim([0, (numpy.hypot(img_size[0], img_size[1]) / 2)])
        pyplot.ylim([(10 ** i) for i in fvu_range])
        ax.set_xticks(r_ticks_orig)
        ax.set_xticklabels([str(int(i)) for i in r_ticks])
        pyplot.xlabel("R (\")", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("m", fontsize=20)
        save_path = path.join(folder_out, 'rfvum')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # FVU map
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(x_out, y_out, s=10, c=fvu_out,
                                 cmap=pyplot.get_cmap('plasma_r'),
                                 norm=colors.LogNorm(
                                     vmin=(10 ** fvu_range_color[0]),
                                     vmax=(10 ** fvu_range_color[1])))
        pyplot.xlim([-xy_buffer, (img_size[0] + xy_buffer)])
        pyplot.ylim([-xy_buffer, (img_size[1] + xy_buffer)])
        ax.set_aspect('equal', 'box')
        pyplot.xlabel("x (px)", fontsize=20)
        pyplot.ylabel("y (px)", fontsize=20)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("FVU", fontsize=20)
        save_path = path.join(folder_out, 'xyfvu')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Magnitude vs FVU
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        pyplot.scatter(m_out, fvu_out, s=5, c='k', label=None)
        ax.set_yscale('log')
        pyplot.xlim(m_range)
        pyplot.ylim([(10 ** i) for i in fvu_range])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        save_path = path.join(folder_out, 'mfvu')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

        # Second magnitude vs FVU
        if fvu_range_2:
            fig = pyplot.figure(figsize=(6, 6.5))
            ax = fig.add_subplot(111)
            ax.set_position([0.25, 0.15, 0.65, 0.65])
            pyplot.scatter(m_out, fvu_out, s=5, c='k', label=None)
            ax.set_yscale('log')
            pyplot.xlim(m_range_2)
            pyplot.ylim([(10 ** i) for i in fvu_range_2])
            pyplot.xlabel("m", fontsize=20)
            pyplot.ylabel("FVU", fontsize=20)
            pyplot.tick_params(labelsize=20)
            save_path = path.join(folder_out, 'mfvu_2')
            pyplot.savefig(save_path + '.png')
            pyplot.savefig(save_path + '.pdf')
            pyplot.close(fig)

        # Magnitude vs FVU, colored by position
        fig = pyplot.figure(figsize=(6, 6.5))
        ax = fig.add_subplot(111)
        ax.set_position([0.25, 0.15, 0.65, 0.65])
        scatter = pyplot.scatter(m_out, fvu_out, s=5, c=r_out,
                                 cmap=pyplot.get_cmap('plasma'), vmin=0,
                                 vmax=(numpy.hypot(img_size[0], img_size[1]) /
                                       2), label=None)
        pyplot.text(0.3, 0.7,
                    r"FVU$_{median}$" + " = {0:.2e}".format(fvu_median_bright),
                    ha='left', transform=fig.transFigure, fontsize=20)
        ax.set_yscale('log')
        pyplot.xlim(m_range)
        pyplot.ylim([(10 ** i) for i in fvu_range])
        pyplot.xlabel("m", fontsize=20)
        pyplot.ylabel("FVU", fontsize=20)
        pyplot.tick_params(labelsize=20)
        colorbar_ax = fig.add_axes([0.25, 0.9, 0.65, 0.03])
        fig.colorbar(scatter, cax=colorbar_ax, orientation='horizontal')
        colorbar_ax.xaxis.set_ticks_position('top')
        colorbar_ax.tick_params(labelsize=20)
        colorbar_ax.set_xlabel("R (px)", fontsize=20)
        save_path = path.join(folder_out, 'mfvur')
        pyplot.savefig(save_path + '.png')
        pyplot.savefig(save_path + '.pdf')
        pyplot.close(fig)

    # End program
    print()
    print("Analysis completed\n")
