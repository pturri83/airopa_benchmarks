from datetime import datetime
import glob
from os import environ, makedirs, path, remove, symlink
import shutil

from astropy import table, time
from astropy.io import ascii, fits
import numpy

from maosi import observation
from maosi.nirc2 import ao_opt_sims


def generate_img(dir_test, dir_label, year, filter_name, atm_inst, pa, parang,
                 rotposn, psf_const, method, psf_reference, satur, bkg, tint,
                 coadds, scene_type, grid_size, grid_mag, date_atm, psf_load):

    # Start the program
    print("\nGeneration started")

    if psf_const:
        psf_const_str = '_flat'
    else:
        psf_const_str = ''

    # Prepare instrument
    nirc2 = ao_opt_sims.NIRC2()
    nirc2.tint = tint
    nirc2.coadds = coadds

    # Prepare the PSF
    dir_airopa = environ['AIROPA_DATA_PATH']
    dir_airopa2 = environ['AIROPA_TEST_DATA']
    
    psf_grid, psf_grid_pos =\
        ao_opt_sims.read_nirc2_psf_grid(path.join(dir_airopa2, 'psf_grids',
                                                  str(year), filter_name,
                                                  atm_inst, str(pa),
                                                  ('psf_grid' + psf_const_str +
                                                   '.fits')),
                                        path.join(dir_airopa2, 'psf_grids',
                                                  str(year), filter_name,
                                                  atm_inst, str(pa),
                                                  'grid_pos.fits'))
    psf = ao_opt_sims.PSF_grid_NIRC2_Kp(psf_grid, psf_grid_pos)

    # Generate the GC scene
    scene = None

    if scene_type == 'gc':
        print('generate_img: Making GC image')
        scene = ao_opt_sims.GCstars(path.join(dir_label, 'label.dat'),
                                    instr=nirc2)
    if scene_type == 'irr_grid':
        print('generate_img: Making IrregularGrid image')
        scene = ao_opt_sims.IrregularGrid(grid_size, grid_mag, nirc2)
        
    elif scene_type == 'grid':
        print('generate_img: Making Grid image')
        scene = ao_opt_sims.Grid(grid_size, grid_mag, instr=nirc2,
                                 noise=0.009952)

    # Generate the GC image
    create_image_folder(dir_test)
    obs = observation.Observation(nirc2, scene, psf, 0, bkg,
                                  origin=(nirc2.array_size / 2), method=method)
    hdr = fits.getheader(path.join(dir_airopa, 'ref_files', ('img_model_' +
                         filter_name + '_' + str(parang) + '_' + str(rotposn) +
                         '.fits')))
    hdr['coadds'] = coadds
    hdr['itime'] = tint
    hdr['mjd-obs'] = time.Time(date_atm, scale='utc').jd1 - 2400000.5
    hdr['rotposn'] = rotposn
    hdr['camname'] = 'narrow'
    hdr['filter'] = filter_name + ' + clear'
    hdr['date-obs'] = date_atm.split()[0] + ' '
    hdr['utc'] = date_atm.split()[1]
    hdr['currinst'] = nirc2.name

    if filter_name is 'Kp':
        hdr['cenwave'] = 2.1245

    hdr['parang'] = parang
    hdr['aofcsalt'] = 90000
    hdr['el'] = 45
    fits.writeto(path.join(dir_test, 'image', 'img_sim.fits'), obs.img, hdr,
                 overwrite=True)

    # Save the catalog
    obs.stars.write(path.join(dir_test, 'image', 'img_sim.cat'), format='ascii',
                    overwrite=True)
    psf_reference_idx = None
    psf_tab = None

    if scene_type == 'gc':
        psf_reference_idx = numpy.where(numpy.array([x == psf_reference for x in
                                        obs.stars['names']]))[0][0]
    elif scene_type == 'grid' or scene_type == 'irr_grid':
        psf_reference_idx = round((grid_size ** 2) / 2)
        obs.stars['names'][psf_reference_idx] = psf_reference
        psf_tab = table.Table([obs.stars['names'], scene.mag, scene.xpos,
                               scene.ypos, ([0.0] * len(scene.name)),
                               ([0.0] * len(scene.name)),
                               ([2009.0] * len(scene.name)),
                               (['-'] * len(scene.name)),
                               ([1] * len(scene.name))],
                              names=('dummy_name1', 'dummy_name2',
                                     'dummy_name3', 'dummy_name4',
                                     'dummy_name5', 'dummy_name6',
                                     'dummy_name7', 'dummy_name8',
                                     'dummy_name9'))

    psf_reference_str = (str(obs.stars['xpix'][psf_reference_idx]) + '   ' +
                         str(obs.stars['ypix'][psf_reference_idx]) + '   # ' +
                         obs.stars['names'][psf_reference_idx] + '\n')

    # Save auxiliary files
    if psf_load:
        bkg_img = obs.img
        bkg_img[:] = (bkg + nirc2.dark_current) * nirc2.tint * nirc2.coadds / \
            nirc2.gain
        fits.writeto(path.join(dir_test, 'image', 'img_sim_back.fits'), bkg_img,
                     hdr, overwrite=True)

        if path.isfile(path.join(dir_test, 'image', 'img_sim_psf.fits')):
            remove(path.join(dir_test, 'image', 'img_sim_psf.fits'))

        symlink(path.join(dir_airopa, 'psf_grids', str(year), filter_name,
                          atm_inst, str(pa), 'psf_single.fits'),
                path.join(dir_test, 'image', 'img_sim_psf.fits'))

    create_psfstars_file(dir_test, dir_airopa, scene_type, psf_tab)
    coo_file = open(path.join(dir_test, 'image', 'img_sim.coo'), 'w')
    coo_file.write(psf_reference_str)
    coo_file.close()

    if path.isfile(path.join(dir_test, 'image', 'grid_pos.fits')):
        remove(path.join(dir_test, 'image', 'grid_pos.fits'))

    symlink(path.join(dir_airopa2, 'psf_grids', str(year), filter_name,
                      atm_inst, str(pa), 'grid_pos.fits'),
            path.join(dir_test, 'image', 'grid_pos.fits'))
    now = datetime.now()
    phase_type = ''

    if atm_inst == 'inst':
        phase_type = 'instrument-only'
    elif atm_inst == 'atm':
        phase_type = 'atmospheric'

    params = open(path.join(dir_test, 'image', 'params_img.txt'), 'w')
    params.write('Date generated:           ' + str(now.year) + '-' +
                 str(now.month) + '-' + str(now.day) + '\n\n')
    params.write('--- PARAMETERS ---' + '\n')
    params.write('Phase map year:           ' + str(year) + '\n')
    params.write('Phase map type:           ' + phase_type + '\n')
    params.write('Filter:                   ' + filter_name + '\n')
    params.write('Parallactic angle (deg):  ' + str(parang) + '\n')
    params.write('Rotator position (deg):   ' + str(rotposn) + '\n')
    params.write('Position angle (deg):     ' + str(pa) + '\n')
    params.write('Constant PSF:             ' + str(psf_const) + '\n')
    params.write('Saturation (DN/px):       ' + str(satur) + '\n')
    params.write('Background (DN/px/s):     ' + str(bkg) + '\n')
    params.write('Integration time (s):     ' + str(tint) + '\n')
    params.write('Coadds:                   ' + str(coadds) + '\n\n')
    params.write('--- OUTPUT ---' + '\n')
    params.write('Simulated stars:          ' + str(len(obs.stars)) + '\n')
    params.close()

    # End program
    print("Generation completed\n")


def create_image_folder(dir_test):

    if not path.exists(path.join(dir_test, 'image')):
        makedirs(path.join(dir_test, 'image'))


def create_image_file(dir_test, dir_obs, file_search='*.fits', prefix=''):

    files = glob.glob(path.join(dir_obs, file_search))

    for i_file, file in enumerate(files):
        print("\rCopying image {0} / {1}...".format((i_file + 1),
              len(files)), end="")
        shutil.copyfile(file, path.join(dir_test, 'image', (prefix +
                        path.basename(file))))
        shutil.copyfile((path.splitext(file)[0] + '.coo'),
                        path.join(dir_test, 'image',
                        (prefix + path.basename(path.splitext(file)[0] +
                         '.coo'))))
        shutil.copyfile((path.splitext(file)[0] + '.max'),
                        path.join(dir_test, 'image',
                        (prefix + path.basename(path.splitext(file)[0] +
                         '.max'))))

    print()


def create_psfstars_file(dir_test, dir_airopa, scene_type, psf_tab):

    if path.isfile(path.join(dir_test, 'image', 'psf_stars.dat')):
        remove(path.join(dir_test, 'image', 'psf_stars.dat'))

    if (scene_type == 'gc') or (scene_type == 'maser'):
        symlink(path.join(dir_airopa, 'ref_files', 'psf_stars.dat'),
                path.join(dir_test, 'image', 'psf_stars.dat'))

    elif scene_type == 'distort':
        symlink(path.join(dir_airopa, 'psf_stars.dat'),
                path.join(dir_test, 'image', 'psf_stars.dat'))

    elif scene_type == 'grid' or scene_type == 'irr_grid':

        if path.isfile(path.join(dir_test, 'image', 'psf_stars.dat')):
            remove(path.join(dir_test, 'image', 'psf_stars.dat'))

        psf_tab.add_row(['TTstar', 15.00, 8.973, 16.97, 0, 0,
                         psf_tab['dummy_name7'][0], '-', 0])
        ascii.write(psf_tab, path.join(dir_test, 'image', 'psf_stars_tmp.dat'),
                    format='no_header')
        psf_file_tmp = open(path.join(dir_test, 'image', 'psf_stars_tmp.dat'),
                            'r')
        psf_file_content = psf_file_tmp.read()
        psf_file_tmp.close()
        remove(path.join(dir_test, 'image', 'psf_stars_tmp.dat'))
        psf_file = open(path.join(dir_test, 'image', 'psf_stars.dat'), 'w')
        psf_file.write('# Name Mag Xarc Yarc Vx Vy t0 Filt PSF' + '\n' +
                       psf_file_content)
        psf_file.close()

    elif scene_type == 'fiber':

        if path.isfile(path.join(dir_test, 'image', 'psf_stars.dat')):
            remove(path.join(dir_test, 'image', 'psf_stars.dat'))

        ascii.write(psf_tab, path.join(dir_test, 'image', 'psf_stars_tmp.dat'),
                    format='no_header')
        psf_file_tmp = open(path.join(dir_test, 'image', 'psf_stars_tmp.dat'),
                            'r')
        psf_file_content = psf_file_tmp.read()
        psf_file_tmp.close()
        remove(path.join(dir_test, 'image', 'psf_stars_tmp.dat'))
        psf_file = open(path.join(dir_test, 'image', 'psf_stars.dat'), 'w')
        psf_file.write('# Name Mag Xarc Yarc Vx Vy t0 Filt PSF' + '\n' +
                       psf_file_content)
        psf_file.close()


def delete_fit_folder(dir_test):

    if path.exists(path.join(dir_test, 'fit')):
        shutil.rmtree(path.join(dir_test, 'fit'))
