"""Prepare folders and files for the on-sky maser mosaic test.

Data parameters:
    dir_test (string) - Test folder
    dir_ce (string) - Observing images folder (center of mosaic)
    dir_nw (string) - Observing images folder (northwest of mosaic)
    dir_ne (string) - Observing images folder (northeast of mosaic)
    dir_sw (string) - Observing images folder (southwest of mosaic)
    dir_se (string) - Observing images folder (southeast of mosaic)
"""

from os import environ

from airopa_test.generate_img import create_image_file, create_image_folder, \
    create_psfstars_file, delete_fit_folder


# Data parameters
dir_test = ('/g/lu/scratch/jlu/work/ao/airopa/AIROPA_TEST/airopa_benchmarks/' +
            'maser')
dir_ce = '/g/lu/data/gc/lgs_data/17maylgs2/clean/msr_C_kp'
dir_nw = '/g/lu/data/gc/lgs_data/17maylgs2/clean/msr_C_NW_kp'
dir_ne = '/g/lu/data/gc/lgs_data/17maylgs2/clean/msr_C_NE_kp'
dir_sw = '/g/lu/data/gc/lgs_data/17maylgs2/clean/msr_C_SW_kp'
dir_se = '/g/lu/data/gc/lgs_data/17maylgs2/clean/msr_C_SE_kp'

# Start the program
print("\nPreparation started")
create_image_folder(dir_test)
create_image_file(dir_test, dir_ce, prefix='ce_')
create_image_file(dir_test, dir_nw, prefix='nw_')
create_image_file(dir_test, dir_ne, prefix='ne_')
create_image_file(dir_test, dir_sw, prefix='sw_')
create_image_file(dir_test, dir_se, prefix='se_')
create_psfstars_file(dir_test, environ['AIROPA_DATA_PATH'], 'maser', None)
delete_fit_folder(dir_test)

# End program
print("Preparation completed\n")
