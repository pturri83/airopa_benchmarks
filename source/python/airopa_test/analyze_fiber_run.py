"""Run AIROPA in different modes on a fiber grid image and produce plots to
compare the profile fitting residuals.

Variables with a name ending in '_2' have the same function of the corresponding
variables without the suffix, and are used to generate a second plot with
different parameters. If a second plot is not needed, use the value 'None' for
all of them.

Data parameters:
    dir_test (string) - Test data folder
    n_fibers (int) - Number of fibers

Plot parameters:
    img_orig_range (float [2]) - Range of values to show on the original image
    img_res_range (float [2]) - Range of values to show on the residual image
    test_pos (int [4, 2]) - Approximate x and y positions of eight bright test
        stars (2 top-left, 2 top-right, 2 bottom-left, 2 bottom-right of the
        image)
    test_r (float) - Radius of the test stars' imaes (px)
    m_range (float [2]) - Range of magnitudes plotted
    m_range_color (float [2]) - Range of magnitudes colored
    m_histmax (int) - Maximum height in the magnitude histogram
    m_histbin (float) - Bin size in the magnitude histogram
    fvu_range (float [2]) - Minimum and maximum FVU shown
    fvu_range_color (float [2]) - Minimum and maximum FVU colored
"""

from airopa_test.analyze_fiber import analyze_fiber


# Data parameters
dir_test = ('/g/lu/scratch/jlu/work/ao/airopa/AIROPA_TEST/airopa_benchmarks/' +
            'grid_fiber_17_17')
n_fibers = 81

# Plot parameters
img_orig_range = [0, 5000]
img_res_range = [-1000, 6000]
test_pos = [[89, 912], [193, 914], [813, 926], [916, 931], [107, 92],
            [209, 93], [828, 102], [932, 104]]
test_r = 30
m_range = [8, 17]
m_range_color = [9, 16]
m_histmax = 200
m_histbin = 0.5
fvu_range = [-4, 1]
fvu_range_2 = [-4, 0]
fvu_range_color = [-3, -1]

# Start program
analyze_fiber(dir_test, n_fibers, img_orig_range, img_res_range, test_pos,
              test_r, m_range, m_range_color, fvu_range, fvu_range_2,
              fvu_range_color, m_histmax, m_histbin)
