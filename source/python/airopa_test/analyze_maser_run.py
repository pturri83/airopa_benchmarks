"""Run AIROPA in different modes on a on-sky maser mosaic image and produce
plots to compare the astrometry and photometry accuracy.

Data parameters:
    dir_test (string) - Test data folder

Analysis parameters:###########################################################update
    n_detect_min (int) - Minimum number of detections for a star to be listed
    bright_mag (float) - Magnitude cut to define "bright" stars. Should be where
        residuals in the 'mdr.png' and 'mdm.png' output plots start to increase
        due to noise

Plot parameters:##############################################################update
    img_orig_range (float [2]) - Range of values to show on the original image
    img_res_range (float [2]) - Range of values to show on the residual image
    test_pos (int [8, 2]) - Approximate x and y positions of eight bright test
        stars (2 top-left, 2 top-right, 2 bottom-left, 2 bottom-right of the
        image)
    test_r (float) - Radius of the test stars' imaes (px)
    m_range (float [2]) - Range of magnitudes plotted
    m_range_color (float [2]) - Range of magnitudes colored
    m_histmax (int) - Maximum height in the magnitude histogram
    m_histbin (float) - Bin size in the magnitude histogram
    m_std_range (float [2]) - Range of magnitude STD plotted
    m_std_range_color (float [2]) - Range of magnitude STD colored
    m_std_text (float) - Exponential magnitude STD of the text
    r_std_range (float [2]) - Range of position STD plotted (px)
    r_std_range_color (float [2]) - Range of position STD colored (px)
    r_std_text (float) - Exponential position STD of the text (px)
    fvu_range (float [2]) - Minimum and maximum FVU shown
    fvu_range_color (float [2]) - Minimum and maximum FVU colored
    fvu_text (float) - Exponential FVU of the text
    quiv_scale (float) - Quiver plot scale
    quiv_legend (float) - Quantity used for the quiver plot legend
    dr_histbin (float) - Bin size in the position difference histogram
    dr_range (float) - Maximum positional difference shown (px)
    dr_histmax (int) - Maximum height in the position difference histogram
    dm_histbin (float) - Bin size in the magnitude difference histogram
    dm_range (float) - Maximum magnitude difference shown (px)
    dm_histmax (int) - Maximum height in the magnitude difference histogram
"""

from airopa_test.analyze_maser import analyze_maser


# Data parameters
dir_test = ('/g/lu/scratch/jlu/work/ao/airopa/AIROPA_TEST/airopa_benchmarks/' +
            'maser')

# Analysis parameters
n_detect_min = 10

# Plot parameters
m_range = [7, 15.5]
dm_range_color = [-0.4, 0.4]
r_std_range = [-1, 0.7]
dr_range = [-2.3, 0.5]
dr_text = -2.2
quiv_scale = 5
quiv_legend = 1
dm_histbin = 0.05
dm_range = 0.6
dm_histmax = 50

# Start program
analyze_maser(dir_test, n_detect_min, m_range, dm_range_color, r_std_range,
              dr_range, dr_text, quiv_scale, quiv_legend, dm_histbin, dm_range,
              dm_histmax)
