"""Analyze the Zernike coefficients of the NIRC2 instrumetal aberrations as
calculated by Olivier Beltramo-Martin.

Data parameters:
    dir_coeff (str) - Folder with the Zernike coefficients
    file_coeff (str) - Zernike coefficients file

Graphic parameters:
    n_coeff (int) - Number of Zernike coefficients to plot together
"""


from os import path

from astropy.io import fits
from matplotlib import colors, pyplot
import numpy as np


# Data parameters
dir_coeff = '/g/lu/data/airopa_data/phase_maps/2017'
file_coeff = 'zernikeCoefficients.fits'

# Graphic parameters
n_coeff = 50

# Load data
print('Program started')
coeff_tmp = fits.open(path.join(dir_coeff, file_coeff))
coeff = coeff_tmp[0].data

# Process data
coeff_abs = np.abs(coeff)
coeff_ns = np.arange(coeff.shape[0])
side = int(np.sqrt(coeff.shape[1]))
directions = [0, (side - 1), (coeff.shape[1] - side), (coeff.shape[1] - 1),
              int(coeff.shape[1] / 2)]
directions_str = ["Top left", "Top rigt", "Bottom left", "Bottom right",
                  "Center"]

# Show plots
pyplot.figure(figsize=(6, 6))

for i_direction in range(5):
    pyplot.plot(coeff_ns, coeff_abs[:, directions[i_direction]],
                label=directions_str[i_direction])
pyplot.xlim([0, n_coeff])
pyplot.legend()

# End program
pyplot.show()
print('Program terminated')
