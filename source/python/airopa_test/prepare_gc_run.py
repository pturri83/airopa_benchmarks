"""Prepare folders and files for the on-sky GC test.

Data parameters:
    dir_test (string) - Test folder
    dir_obs (string) - Observing images folder
    file_search (string) - Names of the image files to analyze
"""

from os import environ

from airopa_test.generate_img import create_image_file, create_image_folder, \
    create_psfstars_file, delete_fit_folder


# Data parameters
dir_test = ('/g/lu/scratch/jlu/work/ao/airopa/AIROPA_TEST/airopa_benchmarks/' +
            'sky')
dir_obs = '/g/lu/data/gc/lgs_data/17auglgs3/clean/kp'
file_search = 'c2???.fits'

# Start the program
print("\nPreparation started")
create_image_folder(dir_test)
create_image_file(dir_test, dir_obs, file_search=file_search)
create_psfstars_file(dir_test, environ['AIROPA_DATA_PATH'], 'gc', None)
delete_fit_folder(dir_test)

# End program
print("Preparation completed\n")
