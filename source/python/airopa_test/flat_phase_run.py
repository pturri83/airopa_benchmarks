"""Create a flat phase map.

Data parameters:
    year (int) - Year of the OTF map
"""

from airopa_test.flat_phase import flat_phase


# Data parameters
year = 2017

# Start the program
flat_phase(year)
