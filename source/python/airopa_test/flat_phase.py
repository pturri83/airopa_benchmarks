from os import environ, path, remove

from astropy.io import fits


def flat_phase(year):

    # Start the program
    print("\nGeneration started")
    dir_airopa = environ['AIROPA_DATA_PATH']

    # Load data
    img = fits.open(path.join(dir_airopa, 'phase_maps', str(year),
                              'phase_maps_reg.fits'))

    # Modify data
    img[0].data[:] = 0

    # Write data
    if path.exists(path.join(dir_airopa, 'phase_maps', str(year),
                             'phase_maps_reg_flat.fits')):
        remove(path.join(dir_airopa, 'phase_maps', str(year),
                         'phase_maps_reg_flat.fits'))

    img.writeto(path.join(dir_airopa, 'phase_maps', str(year),
                          'phase_maps_reg_flat.fits'))

    # End program
    print("Generation completed\n")
