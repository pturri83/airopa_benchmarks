"""Generate a defocus phase map to test if NIRC2 has an incorrect defocus term.

Data parameters:
    dir_year (str) - Year of the model phase folder
    coeff (float) - Defocus Zernike coefficient (nm)
"""

import copy
from os import environ, makedirs, path

from astropy.io import fits
from matplotlib import pyplot
import numpy as np


# Data parameters
dir_year = '2017'
coeff = -50

# Load data
print('Program started')
dir_airopa = environ['AIROPA_DATA_PATH']
phase_orig = fits.open(path.join(dir_airopa, 'phase_maps', dir_year,
                                 'phase_maps_reg.fits'))
phase_size = [phase_orig[0].header['NAXIS1'], phase_orig[0].header['NAXIS2'],
              phase_orig[0].header['NAXIS3']]
pyplot.figure(figsize=(6, 6))
pyplot.imshow(phase_orig[0].data[0, :, :], cmap='jet', aspect='equal')
phase_cen = fits.open(path.join(dir_airopa, 'phase_maps', dir_year, 'cen.fits'))

# Calculate phases
phase_mask = copy.deepcopy(phase_orig[0].data[0, :, :])
phase_mask[np.where(phase_mask)] = 1
x_grid, y_grid = np.meshgrid(np.arange(-(phase_size[1] / 2),
                                       (phase_size[1] / 2), 1),
                             np.arange(-(phase_size[0] / 2),
                                       (phase_size[0] / 2), 1))
x_grid /= phase_size[1] / 2
y_grid /= phase_size[0] / 2
phase_defocus_one = coeff * np.sqrt(3) * (2 * ((x_grid ** 2) + (y_grid ** 2)) -
                                          1)
phase_defocus_one *= phase_mask
pyplot.figure(figsize=(6, 6))
pyplot.imshow(phase_defocus_one, cmap='jet', aspect='equal')
phase_defocus = copy.deepcopy(phase_orig[0].data)

for i_map in range(phase_size[2]):
    phase_defocus[i_map] = phase_defocus_one

phase_final = copy.deepcopy(phase_orig[0].data)
phase_final += phase_defocus
phase_out = copy.deepcopy(phase_orig)

for i_map in range(phase_size[2]):
    phase_out[0].data[i_map, :, :] = phase_final[i_map, :, :]

pyplot.figure(figsize=(6, 6))
pyplot.imshow(phase_final[0, :, :], cmap='jet', aspect='equal')
phase_cen[0].data += phase_defocus_one
std = coeff
pv = coeff * 2 * np.sqrt(3)
print("Defocus STD:       {0:.2f} nm".format(std))
print("Defocus PV:        {0:.2f} nm".format(pv))

# Save files
if not path.exists(path.join(dir_airopa, 'phase_maps', 'defocused', dir_year)):
    makedirs(path.join(dir_airopa, 'phase_maps', 'defocused', dir_year))

phase_out.writeto(path.join(dir_airopa, 'phase_maps', 'defocused', dir_year,
                            'phase_maps_reg_defocus_' + str(coeff) + '.fits'),
                  overwrite=True)
phase_cen.writeto(path.join(dir_airopa, 'phase_maps', 'defocused', dir_year,
                            'cen_defocus_' + str(coeff) + '.fits'),
                  overwrite=True)

# End program
phase_orig.close()
phase_cen.close()
pyplot.show()
print('Program terminated')
