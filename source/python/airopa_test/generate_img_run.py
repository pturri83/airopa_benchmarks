"""Generate NIRC2 simulated images of the GC.

Data parameters:
    dir_test (string) - Test folder
    dir_label (string) - 'label.dat' folder
    year (int) - Year of the OTF map
    filter_name (str) - Name of the filter
    atm_inst (str) - Atmospheric or instrument-only OTF
    pa (int) - Position angle

Analysis parameters:
    psf_const (bool) - Use a constant PSF instead of the variable one to
        generate the image
    psf_load (bool) - Use a loaded PSF instead of generating one
    psf_reference (string) - Name of a reference PSF star in 'label.dat' (case
        sensitive)
    satur (int) - Saturation limit (ADU)
    bkg (float) - Background level (e-/s)
    tint (float) - Integration time (s)
    coadds (int) - Coadds used for the image
    scene_type (string) - Type of scene, either simulated GC ('gc') or grid of
        stars ('grid')
    grid_size (int) - Number of sources on the side of the grid
    grid_mag (float) - Magnitude of sources on the grid
"""

from airopa_test.generate_img import generate_img


# Data parameters
dir_test = ('/g/lu/scratch/jlu/work/ao/airopa/AIROPA_TEST/airopa_benchmarks/' +
            'grid_const')
dir_label = '/g/lu/data/gc/source_list'
year = 2017
filter_name = 'Kp'
atm_inst = 'inst'
pa = 32
parang = -87.7
rotposn = 0.7
date_atm = '2017-08-23 07:00:00'

# Simulation parameters
psf_const = True
psf_load = False
method = 'bilinear'
psf_reference = 'irs16C'
satur = 1e12
bkg = 3
tint = 2.8
coadds = 200
scene_type = 'grid'
grid_size = 7
grid_mag = 12

# Start the program
generate_img(dir_test, dir_label, year, filter_name, atm_inst, pa, parang,
             rotposn, psf_const, method, psf_reference, satur, bkg, tint,
             coadds, scene_type, grid_size, grid_mag, date_atm, psf_load)
